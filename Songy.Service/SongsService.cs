﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class SongsService : IDisposable
    {
        private readonly SongsRepository _repo;
        public SongsService()
        {
            _repo = new SongsRepository();
        }
        public IQueryable<SongsViewModel> GetIQueryableSongs()
        {
            return _repo.GetIQueryableSongs();
        }
        public async Task<ResponseModel<object>> AddUpdate(SongsAddUpdateModel model)
        {
            return await _repo.AddUpdate(model);
        }

        public async Task<SongsAddUpdateModel> GetSongsDetail(long Id)
        {
            return await _repo.GetSongsDetail(Id);
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            return await _repo.Delete(Id);
        }

        public async Task<ResponseModel<object>> GetTrendingSongsRequestsList(TrendingSongsRequestModel model)
        {
            return await _repo.GetTrendingSongsRequestsList(model);
        }
        public async Task<ResponseModel<object>> GetSongsByCategoryRequestsList(SongsByCategoryRequestModel model)
        {
            return await _repo.GetSongsByCategoryRequestsList(model);
        }

        public async Task<ResponseModel<object>> GetSongsBySearchRequestsList(SongsBySearchRequestModel model)
        {
            return await _repo.GetSongsBySearchRequestsList(model);
        }
        public async Task<ResponseModel<object>> SongDownload(SongDownloadRequestModel model)
        {
            return await _repo.SongDownload(model);
        }

        public async Task<ResponseModel<object>> MySongsDownload(MySongsRequestModel model)
        {
            return await _repo.MySongsDownload(model);
        }

        public async Task<ResponseModel<object>> SearchSongsAvailableForPurchase(PurchaseSearchSongRequestModel model)
        {
            return await _repo.SearchSongsAvailableForPurchase(model);
        }
        public async Task<ResponseModel<object>> PurchaseSongs(PurchaseSongRequestModel model)
        {
            return await _repo.PurchaseSongs(model);
        }

        public async Task<ResponseModel<object>> GetMasterSongsDetails(SongsDetailsRequestModel model)
        {
            return await _repo.GetMasterSongsDetails(model);
        }
        public async Task<ResponseModel<object>> GetRequestedSongsDetails(SongsDetailsRequestModel model)
        {
            return await _repo.GetRequestedSongsDetails(model);
        }
        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
