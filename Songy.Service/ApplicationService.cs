﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class ApplicationService : IDisposable
    {
        private ApplicationRepository _repo;

        public ApplicationService()
        {
            _repo = new ApplicationRepository();
        }

        public ApplicationSettingModel GetAppSettings()
        {
            return _repo.GetAppSettings();
        }

        public async Task<ApplicationSettingModel> GetAppSettingAsync()
        {
            return await _repo.GetAppSettingAsync();
        }
        public void Dispose()
        {
            _repo.Dispose();
        }

        public async Task<ResponseModel<object>> UpdateSettings(ApplicationSettingModel model)
        {
            return await _repo.UpdateSettings(model);
        }
    }
}