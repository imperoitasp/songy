﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class CategoryService : IDisposable
    {
        private readonly CategoryRepository _repo;

        public CategoryService()
        {
            _repo = new CategoryRepository();
        }

        public IQueryable<CategoryDropdownModel> GetCategoryDropdown(bool FromSong)
        {
            return _repo.GetCategoryDropdown(FromSong);
        }
        public IQueryable<AdminCategoryModel> GetAllCategory_IQueryable()
        {
            return _repo.GetAllCategory_IQueryable();
        }

        public bool isCategoryDuplicate(AdminCategoryModel model)
        {
            return _repo.isCategoryDuplicate(model);
        }

        public async Task<ResponseModel<object>> AddUpdate(AdminCategoryModel model)
        {
            return await _repo.AddUpdate(model);
        }

        public async Task<ResponseModel<object>> DeleteCategory(long CategoryId)
        {
            return await _repo.Delete(CategoryId);
        }

        public async Task<ResponseModel<object>> GetCategory(string UserId)
        {
            return await _repo.GetCategory(UserId);
        }

        public async Task<AdminCategoryModel> GetCategoryDetail(long CatrgoryId)
        {
            return await _repo.GetCategoryDetail(CatrgoryId);
        }

        public bool IsTitleExist(string CategoryName, long CategoryId)
        {
            return _repo.IsTitleExist(CategoryName, CategoryId);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
