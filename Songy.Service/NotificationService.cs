﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class NotificationService : IDisposable
    {
        private NotificationRepository _repo;

        public NotificationService()
        {
            _repo = new NotificationRepository();
        }

        public void Dispose()
        {
            _repo.Dispose();
        }

        public async Task<ResponseModel<object>> GetNotificationList(NotificationGetModel model)
        {
            return await _repo.GetNotificationList(model);
        }
        public async Task<ResponseModel<object>> ReadNotification(NotificationReadModel model)
        {
            return await _repo.ReadNotification(model);
        }
        public async Task<ResponseModel<object>> NotificationSend(NotificationSendModel model)
        {
            return await _repo.NotificationSend(model);
        }
    }
}
