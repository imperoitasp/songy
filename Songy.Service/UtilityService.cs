﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class UtilityService
    {
        private readonly UtilityRepository _repo;
        public UtilityService()
        {
            _repo = new UtilityRepository();
        }

        public UtilityModel About()
        {
            return _repo.About();
        }

        public async Task<int> Updateabout(UtilityModel model)
        {
            return await _repo.UpdateAbout(model);
        }

        public UtilityModel TermsCondition()
        {
            return _repo.Term();
        }

        public async Task<int> UpdateTermsConditions(UtilityModel model)
        {
            return await _repo.UpdateTermsConditions(model);
        }
        public async Task<ResponseModel<object>> GetAboutUs(string UserId)
        {
            return await _repo.GetAboutUs(UserId);
        }
        public async Task<ResponseModel<object>> GetTerms(string UserId)
        {
            return await _repo.GetTerms(UserId);
        }

        public async Task<ResponseModel<object>> ContactUs(ContactUsModel model)
        {
            return await _repo.ContactUs(model);
        }

        public async Task<AdminDashboardModel> GetAddminDashboard()
        {
            return await _repo.GetAddminDashboard();
        }
    }
}
