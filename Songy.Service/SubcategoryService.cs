﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class SubCategoryService : IDisposable
    {
        public SubCategoryRepository _repo;

        public SubCategoryService()
        {
            _repo = new SubCategoryRepository();
        }

        public async Task<ResponseModel<SubCategoryAddUpdateResultModel>> AddUpdate(SubCategoryAddUpdateRequestModel model)
        {
            return await _repo.AddUpdate(model);
        }

        public bool IsNameExist(string Name, int CateogoryId, int SubCateogoryId)
        {
            return _repo.IsNameExist(Name, CateogoryId, SubCateogoryId);
        }

        public IQueryable<SubCategoryListModel> GetAllSubCategory_IQueryable()
        {
            return _repo.GetAllSubCategory_IQueryable();
        }

        public async Task<SubCategoryAddUpdateRequestModel> GetSubCategoryDetail(long SubCateogoryId)
        {
            return await _repo.GetSubCategoryDetail(SubCateogoryId);
        }

        public async Task<ResponseModel<object>> Delete(long SubCategoryId)
        {
            return await _repo.Delete(SubCategoryId);
        }

        public IQueryable<SubCategoryDropdownModel> GetSubCategoryDropdown(long CategoryId)
        {
            return _repo.GetSubCategoryDropdown(CategoryId);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
