﻿using Microsoft.Owin;
using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class UserService : IDisposable
    {
        private UserRepository _repo;
        private readonly IOwinContext _owin;

        public UserService()
        {
            _repo = new UserRepository();
        }

        public UserService(IOwinContext owin)
        {
            _repo = new UserRepository(owin);
            this._owin = owin;
        }

        public async Task<string> FindUser(string UserName, string Password)
        {
            return await _repo.FindUser(UserName, Password);
        }

        public bool IsValidUser(string NameIdentifier)
        {
            return _repo.IsValidUser(NameIdentifier);
        }

        public bool CheckUserIsActive(string UserId)
        {
            return _repo.CheckUserIsActive(UserId);
        }

        public async Task<ResponseModel<object>> UserSighnUp(UserSignUpModel model)
        {
            return await _repo.UserSighnUp(model);
        }

        public async Task<ResponseModel<object>> ExternalLogin(ExternalConnectModel model)
        {
            return await _repo.ExternalLogin(model);
        }

        public async Task<ResponseModel<object>> UserLoginWithEmail(UserSignInModel model)
        {
            return await _repo.UserLoginWithEmail(model);
        }

        public async Task<ResponseModel<object>> GetMyProfileForEdit(string UserId)
        {
            return await _repo.GetMyProfileForEdit(UserId);
        }

        public async Task<ResponseModel<object>> SaveMyProfile(EditMyProfileSumbitModel model)
        {
            return await _repo.SaveMyProfile(model);
        }
        public async Task<ResponseModel<object>> SaveMyProfileImage(string ProfileImageName, string UserId)
        {
            return await _repo.SaveMyProfileImage(ProfileImageName, UserId);
        }
        public async Task<ResponseModel<object>> SaveMyProfileLanguage(SaveMyProfileLanguageSumbitModel model)
        {
            return await _repo.SaveMyProfileLanguage(model);
        }

        public async Task<ResponseModel<object>> ChangePassword(UserChangePasswordModel model)
        {
            return await _repo.ChangePassword(model);
        }

        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            return await _repo.ForgotPassword(model);
        }

        public async Task<ResponseModel<object>> ForgotPasswordByAdmin(string UserId)
        {
            return await _repo.ForgotPasswordByAdmin(UserId);
        }

        public async Task<ResponseModel<object>> Logout(UserSignoutModel model)
        {
            return await _repo.Logout(model);
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            return await _repo.AdminLogin(model);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            return await _repo.AdminChangePassword(model);
        }
        public async Task<ResponseModel<object>> ResetPassword(ResetPasswordModel model)
        {
            return await _repo.ResetPassword(model);
        }

        public IQueryable<UserDetailsModel> getAllUsers_IQueryable()
        {
            return _repo.getAllUsers_IQueryable();
        }

        public ResponseModel<bool> ActiveToggle(string userId)
        {
            return _repo.ActiveToggle(userId);
        }

        public async Task<ResponseModel<object>> Delete(string UserId)
        {
            return await _repo.Delete(UserId);
        }

        public void WebLogout()
        {
            _repo.WebLogout();
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
