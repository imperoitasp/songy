﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class ContactusService
    {
        private readonly ContactusRepository _repo;
        public ContactusService()
        {
            _repo = new ContactusRepository();
        }
        public IQueryable<ContactUsViewModel> getContactUs_IQueryable()
        {
            return _repo.getContactUs_IQueryable();
        }
    }
}
