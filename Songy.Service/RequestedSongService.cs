﻿using Songy.Core.Model;
using Songy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Service
{
    public class RequestedSongService : IDisposable
    {
        private RequestedSongRepository _repo;

        public RequestedSongService()
        {
            _repo = new RequestedSongRepository();
        }
        public async Task<RequestedSongsAddUpdateModel> GetRequestedSongsDetail(long Id)
        {
            return await _repo.GetRequestedSongsDetail(Id);
        }

        public IQueryable<SongsViewModel> GetIQueryableUploadedSongs()
        {
            return _repo.GetIQueryableUploadedSongs();
        }
        public IQueryable<SongsViewModel> GetIQueryablePendingSongs()
        {
            return _repo.GetIQueryablePendingSongs();
        }

        public IQueryable<SongsViewModel> RequestedBySongs(long SongsId)
        {
            return _repo.RequestedBySongs(SongsId);
        }

        public IQueryable<UserDetailsModel> RequestedSongsDownloadByUser(long SongsId)
        {
            return _repo.RequestedSongsDownloadByUser(SongsId);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateRequestedSongs(RequestedSongsAddUpdateModel model)
        {
            return await _repo.InsertOrUpdateRequestedSongs(model);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
