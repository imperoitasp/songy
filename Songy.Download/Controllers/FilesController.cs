﻿using Songy.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Songy.Download.Controllers
{
    public class FilesController : Controller
    {
        [AllowAnonymous]
        public FileResult Download(string FileName)
        {
            var SongPath = GlobalConfig.RequestedSongsPath + FileName;
            if (System.IO.File.Exists(SongPath))
            {
                return File(SongPath, System.Net.Mime.MediaTypeNames.Application.Octet, FileName);
            }
            return null;
        }
    }
}