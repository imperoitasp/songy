﻿using Microsoft.AspNet.Identity;
using Songy.API.Filter;
using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Songy.API.Controllers
{
    [RoutePrefix("API/Setting")]
    [CustomAuthorization]
    public class SettingController : ApiController
    {
        private UtilityService _service;
        protected ResponseModel<object> mResult;

        public SettingController()
        {
            _service = new UtilityService();
        }

        [HttpPost]
        [Route("ContactUs")]
        public async Task<ResponseModel<object>> ContactUs(ContactUsModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserId = User.Identity.GetUserId();
                return await _service.ContactUs(model);
            }
            else
            {
                string Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return new ResponseModel<object>()
                {
                    Message = Message,
                    Result = model
                };
            }
        }

        [Route("GetAboutUs")]
        public async Task<ResponseModel<object>> GetAboutUs()
        {
            var UserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }
            return await _service.GetAboutUs(UserId);
        }

        [Route("GetTerms")]
        public async Task<ResponseModel<object>> GetTerms()
        {
            var UserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }
            mResult = await _service.GetTerms(UserId);
            return mResult;
        }

        [Route("GetPrivacy")]
        public async Task<ResponseModel<object>> GetPrivacy()
        {
            var UserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }
            mResult = await _service.GetTerms(UserId);
            return mResult;
        }
    }
}
