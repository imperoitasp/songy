﻿using Microsoft.AspNet.Identity;
using Songy.API.Filter;
using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Songy.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Notification")]
    public class NotificationController : ApiController
    {
        private NotificationService _service;

        /// <summary>
        /// Constructor
        /// </summary>
        public NotificationController()
        {
            _service = new NotificationService();
        }

        /// <summary>
        /// Get notification list
        /// </summary>
        /// <returns>ResponseModel</returns>
        [HttpGet]
        [Route("List")]
        public async Task<ResponseModel<object>> NotificationList([FromUri]NotificationGetModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.GetNotificationList(model);

            return mResult;
        }

        /// <summary>
        /// Read selected notification
        /// </summary>
        /// <returns>ResponseModel</returns>
        [HttpGet]
        [Route("Read")]
        public async Task<ResponseModel<object>> ReadNotification([FromUri]NotificationReadModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.ReadNotification(model);

            return mResult;
        }
    }
}
