﻿using Microsoft.AspNet.Identity;
using Songy.API.Filter;
using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Songy.API.Controllers
{
    /// <summary>
    /// API Songs
    /// </summary>
    /// <returns></returns>
    [CustomAuthorization]
    [RoutePrefix("API/Songs")]
    public class SongsController : ApiController
    {

        private readonly SongsService _service;
        /// <summary>
        /// constructor
        /// </summary>
        /// <returns></returns>
        public SongsController()
        {
            _service = new SongsService();
        }

        /// <summary>
        /// Get Trending List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Trending")]
        public async Task<ResponseModel<object>> GetTrendingSongsRequestsList()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            TrendingSongsRequestModel model = new TrendingSongsRequestModel();
            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.GetTrendingSongsRequestsList(model);
            return mResult;
        }

        /// <summary>
        /// Get songs by category List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ByCategory")]
        public async Task<ResponseModel<object>> GetSongsByCategoryRequestsList([FromUri]SongsByCategoryRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.GetSongsByCategoryRequestsList(model);
            return mResult;
        }

        /// <summary>
        /// Get songs by search
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("BySearch")]
        public async Task<ResponseModel<object>> GetSongsBySearchRequestsList([FromUri]SongsBySearchRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.GetSongsBySearchRequestsList(model);
            return mResult;
        }

        /// <summary>
        /// Get My Purchase songs
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("MyPurchase")]
        public async Task<ResponseModel<object>> MySongsDownload([FromUri]MySongsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.MySongsDownload(model);
            return mResult;
        }

        /// <summary>
        /// Download song
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Download")]
        public async Task<ResponseModel<object>> SongDownload([FromUri]SongDownloadRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.SongDownload(model);
            return mResult;
        }

        /// <summary>
        /// save PurchaseSongs
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("PurchaseSongs")]
        public async Task<ResponseModel<object>> PurchaseSongs(PurchaseSongRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.PurchaseSongs(model);
            return mResult;
        }

        /// <summary>
        /// Search existing songs
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchSongsAvailable")]
        public async Task<ResponseModel<object>> SearchSongsAvailableForPurchase(PurchaseSearchSongRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.SearchSongsAvailableForPurchase(model);
            return mResult;
        }

        /// <summary>
        /// Get Requested Songs Details
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RequestedSongsDetails")]
        public async Task<ResponseModel<object>> GetRequestedSongsDetails([FromUri]SongsDetailsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.GetRequestedSongsDetails(model);
            return mResult;
        }

        /// <summary>
        ///  Get master Songs Details
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SongsDetails")]
        public async Task<ResponseModel<object>> GetMasterSongsDetails([FromUri]SongsDetailsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.GetMasterSongsDetails(model);
            return mResult;
        }

    }
}
