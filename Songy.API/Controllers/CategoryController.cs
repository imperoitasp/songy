﻿using Microsoft.AspNet.Identity;
using Songy.API.Filter;
using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Songy.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Category")]
    public class CategoryController : ApiController
    {
        private CategoryService _service;
        public CategoryController()
        {
            _service = new CategoryService();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetCategory")]
        public async Task<ResponseModel<object>> GetCategoryList()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }
            mResult = await _service.GetCategory(UserId);
            return mResult;
        }

    }
}
