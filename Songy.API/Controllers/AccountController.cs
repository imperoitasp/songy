﻿using Songy.API.Filter;
using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Songy.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Account")]
    public class AccountController : ApiController
    {
        private UserService _service;
        /// <summary>
        /// Constructor
        /// </summary>
        public AccountController()
        {
            _service = new UserService();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ExternalLogin")]
        public async Task<ResponseModel<object>> ExternalLogin(ExternalConnectModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.ExternalLogin(model);
            return mResult;
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("UserLogin")]
        public async Task<ResponseModel<object>> UserLoginWithEmail(UserSignInModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.UserLoginWithEmail(model);
            return mResult;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("UserSighnUp")]
        public async Task<ResponseModel<object>> SighnUp(UserSignUpModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.UserSighnUp(model);
            return mResult;
        }

    }
}
