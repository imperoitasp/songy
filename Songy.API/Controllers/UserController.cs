﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Songy.API.Filter;
using Songy.API.Providers;
using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Songy.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Users")]
    public class UserController : ApiController
    {
        private UserService _service;
        /// <summary>
        /// Constructor
        /// </summary>
        public UserController()
        {
            _service = new UserService();
        }


        /// <summary>
        /// Get my profile data fro edit
        /// </summary>
        /// <returns>ResponseModel</returns>
        [HttpGet]
        [Route("EditMyProfile")]
        public async Task<ResponseModel<object>> GetMyProfileForEdit()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var UserId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            mResult = await _service.GetMyProfileForEdit(UserId);
            return mResult;
        }

        /// <summary>
        /// Save my profileImage
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveMyProfileImage")]
        public async Task<ResponseModel<object>> SaveMyProfileImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                StringBuilder modelString = new StringBuilder();
                Dictionary<string, object> dicFormData = new Dictionary<string, object>();

                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());

                //access form data
                NameValueCollection formData = provider.FormData;

                //access files
                IList<HttpContent> files = provider.Files;

                #region save uploaded image
                var ProfileImageName = string.Empty;
                string fileSavePath = String.Empty;
                foreach (HttpContent file in files)
                {
                    //to append any text in filename.
                    string ogFileName = file.Headers.ContentDisposition.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff_") + shortGuid + Path.GetExtension(ogFileName);

                    Stream input = await file.ReadAsStreamAsync();
                    byte[] filearray = await file.ReadAsByteArrayAsync();
                    fileSavePath = Path.Combine(GlobalConfig.UserImagePath, thisFileName);

                    using (Stream fileStream = File.OpenWrite(fileSavePath))
                    {
                        input.CopyTo(fileStream);
                        //close file
                        fileStream.Close();
                    }

                    ProfileImageName = thisFileName;
                }

                #endregion save uploaded images

                var UserId = User.Identity.GetUserId();

                if (string.IsNullOrEmpty(UserId))
                {
                    mResult.Status = ResponseStatus.Unauthorized;
                    mResult.Message = ResponseMessages.UnauthorisedAccess;
                    mResult.Result = new { };
                    return mResult;
                }
                return await _service.SaveMyProfileImage(ProfileImageName, UserId);
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
                mResult.Status = ResponseStatus.Failed;
                mResult.Result = err;
                return mResult;
            }
        }

        /// <summary>
        /// Save my profile
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveMyProfile")]
        public async Task<ResponseModel<object>> SaveMyProfile(EditMyProfileSumbitModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                model.UserId = User.Identity.GetUserId();

                if (string.IsNullOrEmpty(model.UserId))
                {
                    mResult.Status = ResponseStatus.Unauthorized;
                    mResult.Message = ResponseMessages.UnauthorisedAccess;
                    mResult.Result = new { };
                    return mResult;
                }

                Validate(model);

                if (!ModelState.IsValid)
                {
                    mResult.Status = ResponseStatus.Failed;
                    string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    mResult.Message = messages;
                    return mResult;
                }
                return await _service.SaveMyProfile(model);
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
                mResult.Status = ResponseStatus.Failed;
                mResult.Result = err;
                return mResult;
            }
        }

        /// <summary>
        /// Save my profile
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveMyProfileLanguage")]
        public async Task<ResponseModel<object>> SaveMyProfilelanguage(SaveMyProfileLanguageSumbitModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                model.UserId = User.Identity.GetUserId();

                if (string.IsNullOrEmpty(model.UserId))
                {
                    mResult.Status = ResponseStatus.Unauthorized;
                    mResult.Message = ResponseMessages.UnauthorisedAccess;
                    mResult.Result = new { };
                    return mResult;
                }

                Validate(model);

                if (!ModelState.IsValid)
                {
                    mResult.Status = ResponseStatus.Failed;
                    string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    mResult.Message = messages;
                    return mResult;
                }
                return await _service.SaveMyProfileLanguage(model);
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
                mResult.Status = ResponseStatus.Failed;
                mResult.Result = err;
                return mResult;
            }
        }

        /// <summary>
        /// Change existing user's password
        /// </summary>
        /// <returns>ResponseModel</returns>
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ResponseModel<object>> ChangePassword(UserChangePasswordModel model)
        {
            model.UserId = User.Identity.GetUserId();
            Validate(model);
            if (ModelState.IsValid)
            {
                return await _service.ChangePassword(model);
            }
            else
            {
                string Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return new ResponseModel<object>() { Message = Message };
            }
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <returns>ResponseModel</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                return await _service.ForgotPassword(model);
            }
            else
            {
                string Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return new ResponseModel<object>() { Message = Message };
            }
        }

        /// <summary>
        /// Log out existing user
        /// </summary>
        /// <returns>ResponseModel</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Logout")]
        public async Task<ResponseModel<object>> Logout(UserSignoutModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(model.UserId))
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = new { };
                return mResult;
            }

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }
            mResult = await _service.Logout(model);
            return mResult;
        }
    }
}
