﻿using Microsoft.AspNet.Identity;
using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Songy.API.Filter
{
    public class CustomAuthorization : AuthorizeAttribute
    {
        private bool SkipAuthorization(HttpActionContext actionContext)
        {
            Contract.Assert(actionContext != null);

            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                       || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);

            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;
            string UserId = principal?.Identity?.GetUserId();
            bool Skip = SkipAuthorization(actionContext);

            string AppVersion = string.Empty;
            string androidAppVersion = "", iosAppVersion = "";

            ApplicationService _service = new ApplicationService();
            ApplicationSettingModel objSettings = _service.GetAppSettings();

            if (actionContext.Request.Headers.Any(x => x.Key == "AppVersion") && actionContext.Request.Headers.Any(x => x.Key == "Platform"))
            {
                AppVersion = actionContext.Request.Headers.FirstOrDefault(x => x.Key == "AppVersion").Value.FirstOrDefault();
                Platform platform = (Platform)Enum.Parse(typeof(Platform), actionContext.Request.Headers.FirstOrDefault(x => x.Key == "Platform").Value.FirstOrDefault(), true);
                if (platform == Platform.IOS)
                    iosAppVersion = AppVersion;
                else
                    androidAppVersion = AppVersion;

                if (objSettings != null && objSettings.ForceUpdate)
                {
                    if ((platform == Platform.IOS && iosAppVersion != objSettings.iOSVersion) || (platform == Platform.Android && androidAppVersion != objSettings.AndroidVersion))
                    {
                        ResponseModel<object> mResult = new ResponseModel<object>();
                        mResult.Status = ResponseStatus.UpgradeRequired;
                        mResult.Message = ResponseMessages.ForceUpdate;
                        mResult.Result = null;
                        actionContext.Response = new HttpResponseMessage
                        {
                            StatusCode = HttpStatusCode.OK,
                            Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                        };
                    }
                }
            }

            if (objSettings != null && objSettings.Maintenance)
            {
                if (objSettings.Maintenance)
                {
                    ResponseModel<object> mResult = new ResponseModel<object>();
                    mResult.Status = ResponseStatus.ServiceUnavailable;
                    mResult.Message = ResponseMessages.Maintenence;
                    mResult.Result = null;
                    actionContext.Response = new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                    };
                }
            }

            if (string.IsNullOrEmpty(UserId) && Skip == false)
            {
                ResponseModel<object> mResult = new ResponseModel<object>();
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
                mResult.Result = null;
                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                };
            }
            else
            {
                string Controller = actionContext.ControllerContext.ControllerDescriptor.ControllerName;
                string Action = actionContext.ActionDescriptor.ActionName;
                if (Skip == false)
                {
                    UserService _userservice = new UserService();
                    bool isActive = _userservice.CheckUserIsActive(UserId);
                    if (!isActive)
                    {
                        ResponseModel<object> mResult = new ResponseModel<object>();
                        mResult.Result = null;
                        mResult.Status = ResponseStatus.AdminBlocked;
                        mResult.Message = ResponseMessages.AdminBlocked;
                        actionContext.Response = new HttpResponseMessage
                        {
                            ReasonPhrase = ResponseMessages.AdminBlocked,
                            StatusCode = HttpStatusCode.OK,
                            Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                        };
                    }
                }
                //ApplicationService _service = new ApplicationService();
                //ApplicationSettingModel objSettings = _service.GetAppSettings();
                //if (objSettings != null && objSettings.Maintenance)
                //{                    
                //    if (objSettings.Maintenance)
                //    {
                //        ResponseModel<object> mResult = new ResponseModel<object>();
                //        mResult.Status = ResponseStatus.ServiceUnavailable;
                //        mResult.Message = ResponseMessages.ApplicationUnderConstruction;
                //        mResult.iOSVersion = objSettings.iOSVersion;
                //        mResult.AndroidVersion = objSettings.AndroidVersion;
                //        actionContext.Response = new HttpResponseMessage
                //        {
                //            StatusCode = HttpStatusCode.OK,
                //            Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                //        };
                //    }
                //}
            }
        }
    }
}