﻿using Microsoft.Owin.Security.OAuth;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Songy.API.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// override ValidateClientAuthentication
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            string UserID;
            using (UserService _service = new UserService())
            {
                UserID = await _service.FindUser(context.UserName, context.Password);

                if (string.IsNullOrEmpty(UserID))
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            // Retrieves a user name from current login in system use this User.Identity.Name
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));

            // Retrieves a user id from current login in system use this User.Identity.GetUserId()
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, UserID));

            context.Validated(identity);
        }
    }
}