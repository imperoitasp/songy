﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Core.Model
{
    public class ContactUsViewModel
    {
        public string Name { get; set; }

        public string Message { get; set; }

        public DateTime DateTime { get; set; }
    }
}
