﻿using Songy.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Core.Model
{
    public class UserDetailsModel : UserDetailModel
    {
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ExternalConnectModel
    {
        [Display(Name = "FullName")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please provide external connect id")]
        public string ExternalConnectId { get; set; } //ProviderKey

        [Display(Name = "platform")]
        [Required(ErrorMessage = "Please enter {0}")]
        public Platform Platform { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please provide external connect type")]
        public ExternalConnectTypes ExternalConnectType { get; set; } //LoginProvider

        [EmailAddress(ErrorMessage = "Please enter valid email address")]
        [StringLength(256, ErrorMessage = "{0} should be {2} to {1} characters long", MinimumLength = 8)]
        public string EmailID { get; set; }
    }

    public class UserSignInModel
    {

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-\+]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string EmailID { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }

        [Display(Name = "platform")]
        [Required(ErrorMessage = "Please enter {0}")]
        public Platform Platform { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }

        [Display(Name = "user prefer Language")]
        public Language Language { get; set; }

        public ExternalConnectTypes ExternalConnectType { get; set; }

    }

    public class UserSignUpModel
    {

        [Display(Name = "Facebook Id")]
        public string FacebookId { get; set; }

        [Required]
        public bool IsFacebookUser { get; set; }

        [Display(Name = "FullName")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string FullName { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-\+]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string EmailID { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }


        [Display(Name = "platform")]
        [Required(ErrorMessage = "Please enter {0}")]
        public Platform Platform { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }

        [Required]
        public Language Language { get; set; }

        public ExternalConnectTypes ExternalConnectType { get; set; } = ExternalConnectTypes.Normal;

    }

    public class UserSignoutModel
    {
        public string UserId { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }
    }

    public class LoginResponseModel
    {
        public UserDetailModel UserDetail { get; set; }
        public AccessTokenModel AccessToken { get; set; }

    }

    public class UserDetailModel
    {
        public string Id { get; set; }
        public UserDetailModel()
        {
            ProfileImageUrl = FullName = Email = string.Empty;
        }
        public string UserName { get; set; }
        public string Email { get; set; }
        public UserRoles UserRole { get; set; }
        public string FullName { get; set; }
        public string ProfileImageUrl { get; set; }
        public Language UserLanguage { get; set; }
        public ExternalConnectTypes ExternalConnectType { get; set; }
    }

    public class MyProfileResponseModel
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string ProfileImageUrl { get; set; }

    }

    public class EditMyProfileSumbitModel
    {

        public string UserId { get; set; }

        [Display(Name = "FullName")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string FullName { get; set; }

    }

    public class SaveMyProfileLanguageSumbitModel
    {

        public string UserId { get; set; }

        [Display(Name = "Language")]
        [Required(ErrorMessage = "Please chose {0}")]
        public Language Language { get; set; }

    }

    public class UserChangePasswordModel
    {
        public string UserId { get; set; }

        [Display(Name = "current password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string CurrentPassword { get; set; }

        [Display(Name = "new password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "confirm password")]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Comfirm Password does not match with Password")]
        public string ConfirmPassword { get; set; }
    }

    public class UserForgotPasswordModel
    {
        [Display(Name = "email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [EmailAddress(ErrorMessage = "Please enter valid {0}")]
        public string EmailID { get; set; }
    }

    public class ResetPasswordModel
    {

        public string UserId { get; set; }

        [Display(Name = "password reset token")]
        [Required(ErrorMessage = "{0} is required")]
        public string PasswordResetToken { get; set; }

        [Display(Name = "email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [EmailAddress(ErrorMessage = "Please enter valid {0}")]
        public string EmailID { get; set; }

        [Display(Name = "new password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "confirm password")]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Comfirm Password does not match with Password")]
        public string ConfirmPassword { get; set; }

    }
    public class SongDownloadRequestModel
    {
        public string UserId { get; set; }

        [Required]
        public long SongId { get; set; }
    }
}
