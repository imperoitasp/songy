﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Core.Model
{
    public class CategoryDropdownModel
    {
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class AdminCategoryModel
    {
        public AdminCategoryModel()
        {
            ImageName = GlobalConfig.DefaultImage;
            IconName = GlobalConfig.DefaultIcon;
        }
        public long Id { get; set; }

        //[Required(ErrorMessage = "Please upload category image")]
        public string ImageName { get; set; }

        //[Required(ErrorMessage = "Please upload category Icon")]
        public string IconName { get; set; }

        public bool IsSubCateogory { get; set; } = true;

        [Required(ErrorMessage = "Please enter price for android")]
        public string PriceIdAndroid { get; set; }

        [Required(ErrorMessage = "Please enter price for iOS")]
        public string PriceIdiOS { get; set; }

        public int Order { get; set; }

        public virtual HttpPostedFileBase FileBaseImageCategory { get; set; }

        public virtual HttpPostedFileBase FileBaseIconCategory { get; set; }

        public List<CategoryLanguageModel> Categorylanguagemodellist { get; set; }
    }

    public class CategoryLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        public long CategoryId { get; set;}

        [Required(ErrorMessage = "Please enter category name")]
        [Remote("CheckExistingTitle", "Category",AdditionalFields = "CategoryId", HttpMethod = "POST", ErrorMessage = "Name already exists. Please enter a different name.")]
        public string CategoryName { get; set; }

        public virtual string LanguageName { get; set; }
    }

    public class CategoryViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string IconUrl { get; set; }
        public string ImageUrl { get; set; }
        public int SortOrder { get; set; }
    }

    public class CategoryListModel
    {
        public string Text { get; set; }

        public string Value { get; set; }

    }

}
