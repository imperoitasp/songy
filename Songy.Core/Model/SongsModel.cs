﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Songy.Core.Model
{
    public class SongsAddUpdateModel
    {
        public SongsAddUpdateModel()
        {
            imageName = GlobalConfig.SongsDefaultImage;
        }
        public string UserId { get; set; }

        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Lyrics { get; set; }

        public virtual HttpPostedFileBase FileBaseSongs { get; set; }

        [Required(ErrorMessage = "Please Choose song you want to upload")]
        public string FileName { get; set; }

        public string imageName { get; set; }

        [Required(ErrorMessage = "Please select category")]
        public long CategoryId { get; set; }

        public long? SubCategoryId { get; set; }

        public Language LanguageId { get; set; }

        public string SongIdAndroid { get; set; }

        public bool IsFree { get; set; } = true;

        public string SongIdIos { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Example { get; set; }

        public List<SongHashTagModel> SoongHashTag { get; set; }
    }

    public class RequestedSongsAddUpdateModel
    {
        public string UserId { get; set; }

        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Lyrics { get; set; }

        public string imageName { get; set; }


        public virtual HttpPostedFileBase FileBaseSongs { get; set; }
        public string FileName { get; set; }

        [Required(ErrorMessage = "Please select category")]
        public long CategoryId { get; set; }

        [Required(ErrorMessage = "Please select language")]
        public Language LanguageId { get; set; }

        public List<SongHashTagModel> SoongHashTag { get; set; }
    }
    public class SongHashTagModel
    {
        public long Id { get; set; }
        public string HashTag { get; set; }
    }

    public class TrendingSongsRequestModel
    {
        public string UserId { get; set; }

        public TrendingSortBy SortBy { get; set; } = TrendingSortBy.Recent;
    }
    public class MySongsRequestModel: PaginationModel
    {
        public string UserId { get; set; }
    }
    public class SongsByCategoryRequestModel: PaginationModel
    {
        [Required]
        public long CategoryId { get; set; }

        public long SubCategoryId { get; set; }

        public string UserId { get; set; }

        public TrendingSortBy SortBy { get; set; } = TrendingSortBy.Recent;
    }
    public class SongsBySearchRequestModel: PaginationModel
    {
        [Required(ErrorMessage = "Please enter {0}")]
        public string SearchText { get; set; }
        public string UserId { get; set; }

        public TrendingSortBy SortBy { get; set; } = TrendingSortBy.Recent;
    }
    public class SongsViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Lyrics { get; set; }
        public string SongsUrl { get; set; }
        public string ImageUrl { get; set; }
        public string CategoryImageUrl { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        //public string ImageName { get; set; }
        //public string FileName { get; set; }
        public Language LanguageId { get; set; }
        public string SongIdAndroid { get; set; }
        public string SongIdIos { get; set; }
        public string Example { get; set; }
        public int TotalDownload { get; set; }
        public int TotalRequested { get; set; }
        public int TotalPurchase { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<SongHashTagModel> HashTag { get; set; }
        public bool Isuploaded { get; set; }
        public string Price { get; set; }
        public bool IsFree { get; set; }
    }

    public class TrendingSongsRequestViewModel: SongsViewModel
    {

    }

    public class MySongsDownloadRequestViewModel: SongsViewModel
    {
        public string UserTag { get; set; }
        public string SongName { get; set; }
        public string PermaLink { get; set; }
    }

    public class SearchRequestViewModel
    {
        public long SongId { get; set; }
        public long MasterSongId { get; set; }
        public string Title { get; set; }
        public string Lyrics { get; set; }
        public string SongsUrl { get; set; }
        public string ImageUrl { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Language LanguageId { get; set; }
        public string SongIdAndroid { get; set; }
        public string SongIdIos { get; set; }
        public string Example { get; set; }
        public int TotalDownload { get; set; }
        public int TotalPurchase { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<SongHashTagModel> HashTag { get; set; }
    }

    public class PurchaseSearchSongRequestModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public long MasterSongId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public List<string> HashTag { get; set; }

    }

    public class SearchSongResponceModel
    {
        [Required(ErrorMessage = "Please enter {0}")]
        public long MasterSongId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public long SongId { get; set; }

        public bool IsFree { get; set; }

        public bool IsMasterSong { get; set; }
    }

    public class PurchaseSongRequestModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public long MasterSongId { get; set; }

        public long SongId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public List<string> HashTag { get; set; }

        public string Price { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string Token { get; set; }

        public bool IsMasterSong { get; set; }
    }

    public class SongsDetailsRequestModel
    {
        public string UserId { get; set; }

        public long SongId { get; set; }
    }

    public class LatestProductResultModel
    {
        public List<SubCategoryFilterResultModel> SubCategories { get; set; }
    }

    public class SubCategoryFilterResultModel :BaseIdModel
    {
        public string Name { get; set; }

        public List<TrendingSongsRequestViewModel> Songs { get; set; }
    }

}
