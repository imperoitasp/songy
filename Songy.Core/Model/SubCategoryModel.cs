﻿using Songy.Core.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Songy.Core.Model
{
    public class SubCategoryAddUpdateRequestModel : BaseIdModel
    {
        public long CategoryId { get; set; }

        public List<SubCategoryLanguageModel> Languages { get; set; }
    }

    public class SubCategoryAddUpdateResultModel : BaseIdModel
    {
    }

    public class SubCategoryLanguageModel
    {

        public long CategoryId { get; set; }

        public long SubCategoryId { get; set; }

        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please enter sub category name")]
        [Remote("CheckExistingName", "SubCategory", AdditionalFields = "CategoryId,SubCategoryId", HttpMethod = "POST", ErrorMessage = "Name already exists. Please enter a different name.")]
        public string Name { get; set; }

        public string LanguageName { get; set; }
    }

    public class SubCategoryListModel : BaseIdModel
    {
        public long CategoryId { get; set; }

        public string CategoryName { get; set; }

        public List<SubCategoryLanguageList> Languages { get; set; }
    }

    public class SubCategoryLanguageList
    {

        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please enter sub category name")]
        [Remote("CheckExistingName", "SubCategory", AdditionalFields = "CategoryId,SubCategoryId", HttpMethod = "POST", ErrorMessage = "Name already exists. Please enter a different name.")]
        public string Name { get; set; }

        public string LanguageName { get; set; }
    }

    public class SubCategoryDropdownModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
