﻿using Songy.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Core.Model
{
    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }

    public class BreadcrumbModel
    {
        public string actionTitle { get; set; }
        public string actionName { get; set; }
        public string actioncontroller { get; set; }
        public string subActionTitle { get; set; }
    }

    public class UtilityModel
    {
        public long Id { get; set; }

        public string DescriptionEnglish { get; set; }

        public string DescriptionArbic { get; set; }
    }
    public class UploadImageModel
    {
        public string imageUrl { get; set; }
        public string saveFilePath { get; set; }
        public string hdnImageName { get; set; }
    }
    public class SaveImageModel
    {
        public string imgBase64String { get; set; }
        public string saveFilePath { get; set; }
        public string extension { get; set; }
        public string previousImageName { get; set; }
    }

    public class ContactUsModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter message")]
        public string Message { get; set; }
    }
    public class AboutModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter about us")]
        public string AboutUs { get; set; }
    }

    public class TermModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter about us")]
        public string Terms { get; set; }
    }

    public class AdminDashboardModel
    {
        public int TotalUser { get; set; }
        public int TotalSongs { get; set; }
        public int TotalDownload { get; set; }
        public int TotalRequest { get; set; }
        public int TotalPurchase { get; set; }
        public int TotalInquiry { get; set; }
        public int TotalUpload { get; set; }
        public int TotalPending { get; set; }
    }
}
