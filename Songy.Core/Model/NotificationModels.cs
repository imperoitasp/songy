﻿using Songy.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Core.Model
{
    public class LookupViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }
    }

    public class DeviceTokenModel
    {
        public string DeviceToken { get; set; }
        public Platform Platform { get; set; }
    }

    public class NotificationSendModel
    {
        public NotificationSendModel()
        {
        }

        public string NotificationText { get; set; }

        public List<DeviceTokenModel> DeviceTokenList { get; set; }

        public NotificationType NotificationStatus { get; set; }

        public string TablePrimaryID { get; set; }

        public string SenderID { get; set; }

        public string SenderName { get; set; }

        public UserRoles SenderRole { get; set; }

        public int TotalUnreadNotificationCount { get; set; }
        public Platform PlatformId { get; set; }
        public long? SongId { get; set; }
    }

    public class NotificationPreferencesRequestModel
    {
        public string UserId { get; set; }
    }

    public class NotificationPreferencesViewModel : LookupViewModel
    {
        /// <summary>
        /// has user preferred to receive this notification
        /// </summary>
        public bool HasPreferred { get; set; } = true;
    }

    public class NotificationPreferenceRequestModel
    {
        [Required]
        public int NotificationTypeId { get; set; }

        public string UserId { get; set; }
    }

    public class NotificationAddModel
    {
        public NotificationAddModel()
        {
        }

        public string NotificationText { get; set; }

        public NotificationType NotificationTypeId { get; set; }

        public long? SongID { get; set; }

        public string SenderID { get; set; }

        public string ReceiverID { get; set; }

        public string TablePrimaryID { get; set; }

        public string ServerResponse { get; set; }
    }

    public class NotificationGetModel : PaginationModel
    {
        public string UserId { get; set; }
    }
    public class NotificationReadModel
    {
        public string UserId { get; set; }

        [Required]
        public int NotificationId { get; set; }

    }

    public class NotificationViewModel
    {
        public long NotificationID { get; set; }
        public string TablePrimaryID { get; set; }
        public string NotificationText { get; set; }
        public NotificationType NotificationTypeId { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDateTimeUTC { get; set; }
        public bool IsRead { get; set; }
        public string Username { get; set; }
        public UserRoles Role { get; set; }
        public bool IsMy { get; set; }
        public string TimeCaption { get; set; }
    }

    public class UnreadNotificationGetModel
    {
        public string ReceiverID { get; set; }
    }

    public class PendingNotification
    {
        public int FeedCount { get; set; }
        public int PeopleCount { get; set; }
        public int EventCount { get; set; }
        public int GeneralCount { get; set; }
    }
}
