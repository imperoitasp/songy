﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Songy.Core.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ResponseMessages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ResponseMessages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Songy.Core.Resources.ResponseMessages", typeof(ResponseMessages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your account has been blocked, kindly contact songy to know why your account is blocked..
        /// </summary>
        public static string AdminBlocked {
            get {
                return ResourceManager.GetString("AdminBlocked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ago.
        /// </summary>
        public static string Ago {
            get {
                return ResourceManager.GetString("Ago", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are already register with songy please login.
        /// </summary>
        public static string AlredyRegister {
            get {
                return ResourceManager.GetString("AlredyRegister", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact message submitted successfully!.
        /// </summary>
        public static string ContactUsSuccess {
            get {
                return ResourceManager.GetString("ContactUsSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter correct password.
        /// </summary>
        public static string CurrentPasswordWrong {
            get {
                return ResourceManager.GetString("CurrentPasswordWrong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data was not found..
        /// </summary>
        public static string DataNotFound {
            get {
                return ResourceManager.GetString("DataNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to day.
        /// </summary>
        public static string Day {
            get {
                return ResourceManager.GetString("Day", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to days.
        /// </summary>
        public static string Days {
            get {
                return ResourceManager.GetString("Days", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oops! It seems your account has been deactivated, contact us to resolve this issue..
        /// </summary>
        public static string DeactivatedAccount {
            get {
                return ResourceManager.GetString("DeactivatedAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You need to provide email address..
        /// </summary>
        public static string EmailRequired {
            get {
                return ResourceManager.GetString("EmailRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email address already exists..
        /// </summary>
        public static string EmailTaken {
            get {
                return ResourceManager.GetString("EmailTaken", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No record listed..
        /// </summary>
        public static string EmptyList {
            get {
                return ResourceManager.GetString("EmptyList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please latest update your application.
        /// </summary>
        public static string ForceUpdate {
            get {
                return ResourceManager.GetString("ForceUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hour.
        /// </summary>
        public static string Hour {
            get {
                return ResourceManager.GetString("Hour", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hours.
        /// </summary>
        public static string Hours {
            get {
                return ResourceManager.GetString("Hours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid access..
        /// </summary>
        public static string InvalidAccess {
            get {
                return ResourceManager.GetString("InvalidAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid username or password!.
        /// </summary>
        public static string InvalidCredential {
            get {
                return ResourceManager.GetString("InvalidCredential", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid password..
        /// </summary>
        public static string InvalidPassword {
            get {
                return ResourceManager.GetString("InvalidPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid request model.
        /// </summary>
        public static string InvalidRequestModel {
            get {
                return ResourceManager.GetString("InvalidRequestModel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid role for access..
        /// </summary>
        public static string InvalidRole {
            get {
                return ResourceManager.GetString("InvalidRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid email address..
        /// </summary>
        public static string InvalidUsername {
            get {
                return ResourceManager.GetString("InvalidUsername", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to just now.
        /// </summary>
        public static string JustNow {
            get {
                return ResourceManager.GetString("JustNow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logged in successfully!.
        /// </summary>
        public static string LoggedInSuccess {
            get {
                return ResourceManager.GetString("LoggedInSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logout successful!.
        /// </summary>
        public static string Logout {
            get {
                return ResourceManager.GetString("Logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application under construction.
        /// </summary>
        public static string Maintenence {
            get {
                return ResourceManager.GetString("Maintenence", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to minuite.
        /// </summary>
        public static string Minute {
            get {
                return ResourceManager.GetString("Minute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minutes.
        /// </summary>
        public static string Minutes {
            get {
                return ResourceManager.GetString("Minutes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to month.
        /// </summary>
        public static string Month {
            get {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Months.
        /// </summary>
        public static string Months {
            get {
                return ResourceManager.GetString("Months", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no user registered with this email address..
        /// </summary>
        public static string NoUserRegisterForEmail {
            get {
                return ResourceManager.GetString("NoUserRegisterForEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        public static string OK {
            get {
                return ResourceManager.GetString("OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password changed successfully..
        /// </summary>
        public static string PasswordChanged {
            get {
                return ResourceManager.GetString("PasswordChanged", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password Incorrect.
        /// </summary>
        public static string PasswordIncorrect {
            get {
                return ResourceManager.GetString("PasswordIncorrect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your password has been reset successfully..
        /// </summary>
        public static string PasswordReseted {
            get {
                return ResourceManager.GetString("PasswordReseted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password reset email sent successfully..
        /// </summary>
        public static string PasswordResetEmailSent {
            get {
                return ResourceManager.GetString("PasswordResetEmailSent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile updated..
        /// </summary>
        public static string ProfileUpdated {
            get {
                return ResourceManager.GetString("ProfileUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchase Successfuly.
        /// </summary>
        public static string PurchaseSuccess {
            get {
                return ResourceManager.GetString("PurchaseSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to record added successfully..
        /// </summary>
        public static string RecordAdded {
            get {
                return ResourceManager.GetString("RecordAdded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to record deleted successfully..
        /// </summary>
        public static string RecordDeleted {
            get {
                return ResourceManager.GetString("RecordDeleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to record listed..
        /// </summary>
        public static string RecordListed {
            get {
                return ResourceManager.GetString("RecordListed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are registered successfully..
        /// </summary>
        public static string RegistrationSuccess {
            get {
                return ResourceManager.GetString("RegistrationSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Response failed due to error..
        /// </summary>
        public static string ResponseFailed {
            get {
                return ResourceManager.GetString("ResponseFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to second.
        /// </summary>
        public static string Second {
            get {
                return ResourceManager.GetString("Second", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to seconds.
        /// </summary>
        public static string Seconds {
            get {
                return ResourceManager.GetString("Seconds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are logged out..
        /// </summary>
        public static string Signout {
            get {
                return ResourceManager.GetString("Signout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Something went wrong!.
        /// </summary>
        public static string SomethingWrong {
            get {
                return ResourceManager.GetString("SomethingWrong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your song is available,please request it and access it from &quot;My Purchase&quot; Screen.
        /// </summary>
        public static string SongAvailableForDownload {
            get {
                return ResourceManager.GetString("SongAvailableForDownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your song is available,please purchase it and access it from &quot;My Purchase&quot; Screen.
        /// </summary>
        public static string SongAvailableForPurchase {
            get {
                return ResourceManager.GetString("SongAvailableForPurchase", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your song is not available,please request and access it from &quot;My Purchase&quot; Screen in 2-3 Days..
        /// </summary>
        public static string SongNotAvailableForDownload {
            get {
                return ResourceManager.GetString("SongNotAvailableForDownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your song is not available,please purchase it and access it from &quot;My Purchase&quot; Screen in 2-3 Days..
        /// </summary>
        public static string SongNotAvailableForPurchase {
            get {
                return ResourceManager.GetString("SongNotAvailableForPurchase", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Song purchased, please view it in the download screen..
        /// </summary>
        public static string SongsAlreadyBuy {
            get {
                return ResourceManager.GetString("SongsAlreadyBuy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are unauthorized to access this resource.
        /// </summary>
        public static string UnauthorisedAccess {
            get {
                return ResourceManager.GetString("UnauthorisedAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User not found..
        /// </summary>
        public static string UserNotFound {
            get {
                return ResourceManager.GetString("UserNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        public static string WelcomeRegister {
            get {
                return ResourceManager.GetString("WelcomeRegister", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to year.
        /// </summary>
        public static string Year {
            get {
                return ResourceManager.GetString("Year", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to years.
        /// </summary>
        public static string Years {
            get {
                return ResourceManager.GetString("Years", resourceCulture);
            }
        }
    }
}
