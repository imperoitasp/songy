﻿using Songy.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Songy.Core.Enumerations;
using Songy.Core.Resources;

namespace Songy.Core.Helper
{
    public static class Utilities
    {
        public static ResponseModel<object> SaveCropImage(string imgBase64String, string saveFilePath, string extension)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                imgBase64String = imgBase64String.Replace("data:image/png;base64,", "");

                byte[] bytes = Convert.FromBase64String(imgBase64String);

                Image image;
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }
                if (extension.ToLower() != "png")
                    ImageOptimize.OptimizeSaveJpeg(saveFilePath, image, 90);
                else
                {
                    image.Save(saveFilePath);
                }
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Image uploaded ";
            }
            catch (Exception err)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = err.Message;
            }

            return mResult;
        }

        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.UtcNow - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} {1} " + ResponseMessages.Ago,
                years, years == 1 ? ResponseMessages.Year : ResponseMessages.Years);
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("{0} {1} " + ResponseMessages.Ago,
                months, months == 1 ? ResponseMessages.Month : ResponseMessages.Months);
            }
            if (span.Days > 0)
                return String.Format("{0} {1} " + ResponseMessages.Ago,
                span.Days, span.Days == 1 ? ResponseMessages.Day : ResponseMessages.Days);
            if (span.Hours > 0)
                return String.Format("{0} {1} " + ResponseMessages.Ago,
                span.Hours, span.Hours == 1 ? ResponseMessages.Hour : ResponseMessages.Hours);
            if (span.Minutes > 0)
                return String.Format("{0} {1} " + ResponseMessages.Ago,
                span.Minutes, span.Minutes == 1 ? ResponseMessages.Minute : ResponseMessages.Minutes);
            if (span.Seconds > 5)
                return String.Format("{0} {1} " + ResponseMessages.Ago,
                span.Seconds, span.Seconds == 1 ? ResponseMessages.Second : ResponseMessages.Seconds);
            if (span.Seconds <= 5)
                return ResponseMessages.JustNow;
            return string.Empty;
        }
    }
}
