﻿using Songy.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Core.Helper
{
    public static class GlobalConfig
    {
        public const string ProjectName = "Songy";
        public const string DateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        public const string DisplayDateOnlyFormat = "MM/dd/yyyy";
        public const string FacebookUsernamePrefix = "FB";
        public const string FacebookPasswordSuffix = "Songy";

        #region Path and URL
        public static string baseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static string baseRoutePath = ConfigurationManager.AppSettings["BaseRoutePath"];

        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string fileBaseUrl = baseUrl + "Files/";

        public static string SongsImagetPath = fileRoutePath + @"SongsImage\";
        public static string SongsImageUrl = fileBaseUrl + "SongsImage/";

        public static string SongsPath = fileRoutePath + @"Songs\";
        public static string SongsUrl = fileBaseUrl + "Songs/";
        public static string SongsDefaultImage = "Default.png";

        public static string DefaultImage = "Default.png";
        public static string DefaultIcon = "Default.png";


        public static string RequestedSongsPath = fileRoutePath + @"RequestedSongs\";
        public static string RequestedSongsUrl = fileBaseUrl + "RequestedSongs/";

        public static string CeaseAndDesistPath = fileRoutePath + @"CeaseAndDesistFile\";
        public static string CeaseAndDesistUrl = fileBaseUrl + "CeaseAndDesistFile/";

        public static string EmailTemplatePath = fileRoutePath + @"EmailTemplate\";
        public static string EmailTemplateUrl = fileBaseUrl + "EmailTemplate/";

        public static string UserImagePath = fileRoutePath + @"UserImages\";
        public static string UserImageUrl = fileBaseUrl + "UserImages/";
        public static string UserDefaultImage = "UserDefaultImage.png";

        public static string CategoryIconPath = fileRoutePath + @"CategoryIcon\";
        public static string CategoryIconUrl = fileBaseUrl + "CategoryIcon/";
        public static string CategoryIconDefaultImage = "CategoryIcon.png";

        public static string CategoryImagePath = fileRoutePath + @"CategoryImage\";
        public static string CategoryImageUrl = fileBaseUrl + "CategoryImage/";
        public static string CategoryImageDefaultImage = "CategoryImage.png";

        public static string ForgetPasswordUrl = baseUrl + "Admin/Account/ResetPassword/?UserId=";
        public static bool isLocalServer = false;
        public static string WebBaseUrl = isLocalServer ? "http://192.168.0.109/Songy/" : "http://songy.imperoserver.in/";
        public static string SongsParmaLinkBaseUrl = baseUrl + "Download/";
        #endregion
        public const int PageSize_AdminPanel = 10;
        public const int PageSize = 10;
        public const int NotificationPageSize = 1000;

        public const Language defaultLanguageId = Language.English;

        #region Email
        public static string EmailUserName = ConfigurationManager.AppSettings["EmailUserName"].ToString();
        public static string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
        public static string SMTPClient = ConfigurationManager.AppSettings["SMTPClient"].ToString();
        #endregion

        #region Notification
        public static string FirebaseLegacyServerKey = ConfigurationManager.AppSettings["FirebaseLegacyServerKey"].ToString();
        public static string FirebaseServerKey = ConfigurationManager.AppSettings["FirebaseServerKey"].ToString();
        public static string FirebaseProjectID = ConfigurationManager.AppSettings["FirebaseProjectID"].ToString();
        #endregion

    }
}
