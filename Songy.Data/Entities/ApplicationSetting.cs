﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class ApplicationSetting : BaseIdEntity
    {
        public bool Maintenance { get; set; }
        public bool ForceUpdate { get; set; }

        public string IOSVersion { get; set; }
        public string AndroidVersion { get; set; }
    }
}
