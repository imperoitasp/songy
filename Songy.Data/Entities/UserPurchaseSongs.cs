﻿using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class UserPurchaseSongs : BaseIdEntity
    {
        public long RequestedSongsId { get; set; }

        [ForeignKey("RequestedSongsId")]
        public virtual UserRequestSongs UserRequestSongs { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public string Price { get; set; }

        public string Token { get; set; }

        public DateTime PurchaseDate { get; set; } = Utility.GetSystemDateTimeUTC();

        public virtual ICollection<PurchaseSongHashTag> HashTag { get; set; }

    }
}
