﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class TermsSongy : BaseIdEntity
    {
        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string TermsEnglish { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        public string TermsArbic { get; set; }
    }
}
