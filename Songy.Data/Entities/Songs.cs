﻿using Songy.Core.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Songy.Data.Entities
{
    public class Songs: BaseEntity
    {
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public long? SubCategoryId { get; set; }
        [ForeignKey("SubCategoryId")]
        public virtual SubCategory SubCategory { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string ImageName { get; set; }

        [Required]
        public string FileName { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Lyrics { get; set; }

        public string SongIdAndroid { get; set; }

        public string SongIdIos { get; set; }

        public bool IsFree { get; set; } = false;

        public bool IsDeleted { get; set; } = false;

        [Required]
        [EnumDataType(typeof(Language))]
        public Language LanguageId { get; set; }

        [Required]
        public string Example { get; set; }
        public virtual ICollection<MasterSongsHashTag> HashTag { get; set; }
        public virtual ICollection<UserSongsDownload> SongsDownload { get; set; }
        public virtual ICollection<UserRequestSongs> RequestSongs { get; set; }



    }

}
