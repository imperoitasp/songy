﻿using Songy.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class UserNotifications : BaseEntity
    {
        [EnumDataType(typeof(NotificationType))]
        public NotificationType NotificationType { get; set; }

        [Required]
        public string SenderID { get; set; }
        [ForeignKey("SenderID")]
        public virtual ApplicationUser SenderUser { get; set; }

        [Required]
        public string ReceiverID { get; set; }
        [ForeignKey("ReceiverID")]
        public virtual ApplicationUser ReceiverUser { get; set; }

        public long? SongsID { get; set; }
        [ForeignKey("SongsID")]
        public virtual Songs Songs { get; set; }

        public string TablePrimaryID { get; set; }

        [Required]
        [StringLength(500)]
        public string NotificationText { get; set; }

        [Required]
        public string ServerResponse { get; set; }

        public bool IsRead { get; set; }
    }
}
