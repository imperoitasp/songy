﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class SongHashTag : BaseIdEntity
    {
        public string Name { get; set; }

        public long RequestSongsId { get; set; }

        [ForeignKey("RequestSongsId")]
        public virtual UserRequestSongs RequestSongs { get; set; }
    }
}
