﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class UserRequestSongs : BaseEntity
    {
        public long SongsId { get; set; }

        [ForeignKey("SongsId")]
        public virtual Songs Songs { get; set; }

        public string FileName { get; set; }

        public bool IsUploaded { get; set; } = false;

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Lyrics { get; set; }

        public virtual ICollection<SongHashTag> HashTag { get; set; }
        public virtual ICollection<UserPurchaseSongs> UserPurchaseSongs { get; set; }

    }

}
