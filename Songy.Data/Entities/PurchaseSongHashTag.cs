﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class PurchaseSongHashTag : BaseIdEntity
    {
        [Required]
        public string HashTag { get; set; }

        public long PurchaseId { get; set; }

        [ForeignKey("PurchaseId")]
        public virtual UserPurchaseSongs PurchaseSongs { get; set; }
    }
}
