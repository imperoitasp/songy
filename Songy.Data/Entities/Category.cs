﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Songy.Data.Entities
{
    public class Category : BaseIdEntity
    {
        [Required]
        public string IconName { get; set; }

        [Required]
        public string ImageName { get; set; }

        public int Order { get; set; }

        public bool IsSubCateogory { get; set; }

        public string PriceIdAndroid { get; set; }

        public string PriceIdiOS { get; set; }

        public virtual ICollection<CategoryLanguage> CategoryLanguage { get; set; }

        public virtual ICollection<SubCategory> SubCategorys { get; set; }
    }
}
