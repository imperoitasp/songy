﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class MasterSongsHashTag : BaseIdEntity
    {
        public string HashTag { get; set; }

        public long SongsId { get; set; }

        [ForeignKey("SongsId")]
        public virtual Songs Songs { get; set; }
    }
}
