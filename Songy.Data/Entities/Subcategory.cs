﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Songy.Data.Entities
{
    public class SubCategory : BaseIdEntity
    {
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual ICollection<SubCategoryLanguage> SubCategoryLanguage { get; set; }
        public virtual ICollection<Songs> Songs { get; set; }
        
    }
}
