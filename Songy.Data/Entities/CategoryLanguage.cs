﻿using Songy.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class CategoryLanguage : BaseIdEntity
    {
        public long CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [EnumDataType(typeof(Language))]
        public Language Language { get; set; }

        [Required]
        public string CategoryName { get; set; }
    }
}
