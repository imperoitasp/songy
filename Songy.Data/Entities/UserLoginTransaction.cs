﻿using Songy.Core.Enumerations;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class UserLoginTransaction
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser UserMaster { get; set; }

        [Required]
        public string DeviceToken { get; set; }

        [Required]
        [EnumDataType(typeof(Platform))]
        public Platform Platform { get; set; }

        public bool IsLogout { get; set; }

        public DateTime LoginDateTime { get; set; } = Utility.GetSystemDateTimeUTC();

        public DateTime? LogoutDateTime { get; set; }
    }
}
