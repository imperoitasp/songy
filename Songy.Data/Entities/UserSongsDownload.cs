﻿using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Entities
{
    public class UserSongsDownload: BaseIdEntity
    {
        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public long RequestSongsId { get; set; }

        [ForeignKey("RequestSongsId")]
        public virtual UserRequestSongs RequestSongs { get; set; }

        public DateTime CreatedDate { get; set; } = Utility.GetSystemDateTimeUTC();
    }
}
