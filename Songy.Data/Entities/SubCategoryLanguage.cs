﻿using Songy.Core.Enumerations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Songy.Data.Entities
{
    public class SubCategoryLanguage : BaseIdEntity
    {
        public long SubCategoryId { get; set; }

        [ForeignKey("SubCategoryId")]
        public virtual SubCategory SubCategory { get; set; }

        [EnumDataType(typeof(Language))]
        public Language Language { get; set; }

        [Required]
        public string Name { get; set; }

    }
}
