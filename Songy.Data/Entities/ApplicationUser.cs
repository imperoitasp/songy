﻿using Microsoft.AspNet.Identity.EntityFramework;
using Songy.Core.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Songy.Data.Entities
{
    public class ApplicationUser: IdentityUser
    {
        /// <summary>
        /// Name: My name
        /// Appears in Edit Profile
        /// </summary>
        public string FirstName { get; set; }

        public override string Email { get; set; }

        public UserRoles UserRole { get; set; }

        /// <summary>
        /// Profile image name
        /// </summary>
        [StringLength(500)]
        public string ProfileImageName { get; set; }

        [EnumDataType(typeof(Gender))]
        public Gender GenderId { get; set; } = Gender.Male;

        [EnumDataType(typeof(Language))]
        public Language Language { get; set; } = Language.English;

        [EnumDataType(typeof(ExternalConnectTypes))]
        public ExternalConnectTypes ExternalConnectType { get; set; } = ExternalConnectTypes.None;

        public bool IsActive { get; set; } = true;

        public ICollection<UserLoginTransaction> UserLoginTransactions { get; set; }
    }
}
