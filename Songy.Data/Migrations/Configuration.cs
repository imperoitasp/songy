namespace Songy.Data.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using Songy.Core.Enumerations;
    using Songy.Data.Entities;
    using Songy.Data.Helper;
    using Songy.Data.Identity;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Songy.Data.Helper.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));
            var userManager = new ApplicationUserManager(new ApplicationStore(context));

            #region Add Admin Role
            {
                var role = roleManager.FindByNameAsync(UserRoles.Admin.ToString()).Result;
                if (role == null)
                {
                    role = new IdentityRole(UserRoles.Admin.ToString());
                    var roleresult = roleManager.CreateAsync(role).Result;
                }
            }
            #endregion

            #region Add Mobile User Role
            {
                var role = roleManager.FindByNameAsync(UserRoles.MobileUser.ToString()).Result;
                if (role == null)
                {
                    role = new IdentityRole(UserRoles.MobileUser.ToString());
                    var roleresult = roleManager.CreateAsync(role).Result;
                }
            }
            #endregion

            #region Add admin as a admin@routehack.com
            {
                const string emailId = "admin@Songy.com";
                const string userName = "Admin";
                const string password = "Admin@123";
                ApplicationUser user = userManager.FindByNameAsync(userName).Result;
                if (user == null)
                {
                    user = new ApplicationUser() { FirstName = "Admin", UserName = userName, Email = emailId, UserRole = UserRoles.Admin, Language = Language.English };
                    var result = userManager.CreateAsync(user, password).Result;
                    result = userManager.SetLockoutEnabledAsync(user.Id, false).Result;
                }

                var rolesForUser = userManager.GetRolesAsync(user.Id).Result;
                if (!rolesForUser.Contains(UserRoles.Admin.ToString()))
                {
                    var result = userManager.AddToRoleAsync(user.Id, UserRoles.Admin.ToString()).Result;
                }
            }
            #endregion
        }
    }
}
