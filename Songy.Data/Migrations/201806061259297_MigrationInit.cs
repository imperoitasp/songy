namespace Songy.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AboutSongy",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AboutEnglish = c.String(nullable: false),
                        AboutArbic = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationSetting",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Maintenance = c.Boolean(nullable: false),
                        ForceUpdate = c.Boolean(nullable: false),
                        IOSVersion = c.String(),
                        AndroidVersion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IconName = c.String(nullable: false),
                        ImageName = c.String(nullable: false),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryLanguage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryId = c.Long(nullable: false),
                        Language = c.Int(nullable: false),
                        CategoryName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.ContactUs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Message = c.String(unicode: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ApplicationUser",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        FacebookId = c.String(maxLength: 255),
                        Email = c.String(maxLength: 256),
                        UserRole = c.Int(nullable: false),
                        ProfileImageName = c.String(maxLength: 500),
                        GenderId = c.Int(nullable: false),
                        Language = c.Int(nullable: false),
                        IsRegister = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsFacebookUser = c.Boolean(nullable: false),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.RoleMaster", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserLoginTransaction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        DeviceToken = c.String(nullable: false),
                        Platform = c.Int(nullable: false),
                        IsLogout = c.Boolean(nullable: false),
                        LoginDateTime = c.DateTime(nullable: false),
                        LogoutDateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.MasterSongsHashTag",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        HashTag = c.String(),
                        SongsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Songs", t => t.SongsId, cascadeDelete: true)
                .Index(t => t.SongsId);
            
            CreateTable(
                "dbo.Songs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryId = c.Long(nullable: false),
                        Title = c.String(nullable: false),
                        ImageName = c.String(nullable: false),
                        FileName = c.String(nullable: false),
                        Lyrics = c.String(),
                        SongIdAndroid = c.String(nullable: false),
                        SongIdIos = c.String(nullable: false),
                        LanguageId = c.Int(nullable: false),
                        Example = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.UserRequestSongs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SongsId = c.Long(nullable: false),
                        FileName = c.String(),
                        IsUploaded = c.Boolean(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Lyrics = c.String(unicode: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Songs", t => t.SongsId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: false)
                .Index(t => t.SongsId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SongHashTag",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        RequestSongsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRequestSongs", t => t.RequestSongsId, cascadeDelete: true)
                .Index(t => t.RequestSongsId);
            
            CreateTable(
                "dbo.UserPurchaseSongs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RequestedSongsId = c.Long(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Price = c.Single(nullable: false),
                        PurchaseDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.UserRequestSongs", t => t.RequestedSongsId, cascadeDelete: false)
                .Index(t => t.RequestedSongsId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.PurchaseSongHashTag",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        HashTag = c.String(nullable: false),
                        PurchaseId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserPurchaseSongs", t => t.PurchaseId, cascadeDelete: true)
                .Index(t => t.PurchaseId);
            
            CreateTable(
                "dbo.UserSongsDownload",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        RequestSongsId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Songs_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRequestSongs", t => t.RequestSongsId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.Songs", t => t.Songs_Id)
                .Index(t => t.UserId)
                .Index(t => t.RequestSongsId)
                .Index(t => t.Songs_Id);
            
            CreateTable(
                "dbo.RoleMaster",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.TermsSongy",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TermsEnglish = c.String(nullable: false),
                        TermsArbic = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        NotificationType = c.Int(nullable: false),
                        SenderID = c.String(nullable: false, maxLength: 128),
                        ReceiverID = c.String(nullable: false, maxLength: 128),
                        SongsID = c.Long(),
                        TablePrimaryID = c.String(),
                        NotificationText = c.String(nullable: false, maxLength: 500),
                        ServerResponse = c.String(nullable: false),
                        IsRead = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.ReceiverID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.SenderID, cascadeDelete: false)
                .ForeignKey("dbo.Songs", t => t.SongsID)
                .Index(t => t.SenderID)
                .Index(t => t.ReceiverID)
                .Index(t => t.SongsID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserNotifications", "SongsID", "dbo.Songs");
            DropForeignKey("dbo.UserNotifications", "SenderID", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserNotifications", "ReceiverID", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.RoleMaster");
            DropForeignKey("dbo.UserSongsDownload", "Songs_Id", "dbo.Songs");
            DropForeignKey("dbo.UserSongsDownload", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserSongsDownload", "RequestSongsId", "dbo.UserRequestSongs");
            DropForeignKey("dbo.UserPurchaseSongs", "RequestedSongsId", "dbo.UserRequestSongs");
            DropForeignKey("dbo.UserPurchaseSongs", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.PurchaseSongHashTag", "PurchaseId", "dbo.UserPurchaseSongs");
            DropForeignKey("dbo.UserRequestSongs", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserRequestSongs", "SongsId", "dbo.Songs");
            DropForeignKey("dbo.SongHashTag", "RequestSongsId", "dbo.UserRequestSongs");
            DropForeignKey("dbo.MasterSongsHashTag", "SongsId", "dbo.Songs");
            DropForeignKey("dbo.Songs", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.ContactUs", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserLoginTransaction", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.CategoryLanguage", "CategoryId", "dbo.Category");
            DropIndex("dbo.UserNotifications", new[] { "SongsID" });
            DropIndex("dbo.UserNotifications", new[] { "ReceiverID" });
            DropIndex("dbo.UserNotifications", new[] { "SenderID" });
            DropIndex("dbo.RoleMaster", "RoleNameIndex");
            DropIndex("dbo.UserSongsDownload", new[] { "Songs_Id" });
            DropIndex("dbo.UserSongsDownload", new[] { "RequestSongsId" });
            DropIndex("dbo.UserSongsDownload", new[] { "UserId" });
            DropIndex("dbo.PurchaseSongHashTag", new[] { "PurchaseId" });
            DropIndex("dbo.UserPurchaseSongs", new[] { "UserId" });
            DropIndex("dbo.UserPurchaseSongs", new[] { "RequestedSongsId" });
            DropIndex("dbo.SongHashTag", new[] { "RequestSongsId" });
            DropIndex("dbo.UserRequestSongs", new[] { "UserId" });
            DropIndex("dbo.UserRequestSongs", new[] { "SongsId" });
            DropIndex("dbo.Songs", new[] { "CategoryId" });
            DropIndex("dbo.MasterSongsHashTag", new[] { "SongsId" });
            DropIndex("dbo.UserLoginTransaction", new[] { "UserId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.ApplicationUser", "UserNameIndex");
            DropIndex("dbo.ContactUs", new[] { "UserId" });
            DropIndex("dbo.CategoryLanguage", new[] { "CategoryId" });
            DropTable("dbo.UserNotifications");
            DropTable("dbo.TermsSongy");
            DropTable("dbo.RoleMaster");
            DropTable("dbo.UserSongsDownload");
            DropTable("dbo.PurchaseSongHashTag");
            DropTable("dbo.UserPurchaseSongs");
            DropTable("dbo.SongHashTag");
            DropTable("dbo.UserRequestSongs");
            DropTable("dbo.Songs");
            DropTable("dbo.MasterSongsHashTag");
            DropTable("dbo.UserLoginTransaction");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.ApplicationUser");
            DropTable("dbo.ContactUs");
            DropTable("dbo.CategoryLanguage");
            DropTable("dbo.Category");
            DropTable("dbo.ApplicationSetting");
            DropTable("dbo.AboutSongy");
        }
    }
}
