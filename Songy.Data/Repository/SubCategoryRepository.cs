﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class SubCategoryRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public SubCategoryRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<ResponseModel<SubCategoryAddUpdateResultModel>> AddUpdate(SubCategoryAddUpdateRequestModel model)
        {
            ResponseModel<SubCategoryAddUpdateResultModel> mResult = new ResponseModel<SubCategoryAddUpdateResultModel>() { Message = ResponseMessages.DataNotFound };

            SubCategory entity = await _ctx.SubCategory.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (entity == null)
            {
                mResult.Message = $"Sub category record added successfully";
                entity = new SubCategory();
            }
            else
            {
                mResult.Message = $"Sub category record updated successfully";
            }
            entity.CategoryId = model.CategoryId;


            if (entity.SubCategoryLanguage == null)
            {
                entity.SubCategoryLanguage = new List<SubCategoryLanguage>();
            }
            if (entity.SubCategoryLanguage.Any())
            {
                _ctx.SubCategoryLanguage.RemoveRange(entity.SubCategoryLanguage);
            }

            if (model.Languages != null)
            {
                foreach (SubCategoryLanguageModel item in model.Languages)
                {
                    entity.SubCategoryLanguage.Add(new SubCategoryLanguage() { Language = item.Language, Name = item.Name });
                }
            }
            if (entity.Id == 0)
            {
                _ctx.SubCategory.Add(entity);
            }
            try
            {
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
            }
            catch (DbEntityValidationException err)
            {
                foreach (DbEntityValidationResult validationErrors in err.EntityValidationErrors)
                {
                    foreach (DbValidationError validationError in validationErrors.ValidationErrors)
                    {
                        if (!string.IsNullOrWhiteSpace(mResult.Message))
                        {
                            mResult.Message += Environment.NewLine;
                        }
                        mResult.Message += $"Property: {validationError.PropertyName}, Error: {validationError.ErrorMessage}";
                    }
                }
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }
            return mResult;
        }

        public IQueryable<SubCategoryListModel> GetAllSubCategory_IQueryable()
        {
            return (from c in _ctx.SubCategory
                    select new SubCategoryListModel
                    {
                        Id = c.Id,
                        CategoryId = c.CategoryId,
                        CategoryName = c.Category.CategoryLanguage.FirstOrDefault(x => x.CategoryId == c.CategoryId).CategoryName,
                        Languages = c.SubCategoryLanguage.Select(x => new SubCategoryLanguageList
                        {
                            Language = x.Language,
                            LanguageName = x.Language.ToString(),
                            Name = x.Name
                        }).ToList()
                    }).OrderBy(x => x.Id);
        }

        public async Task<SubCategoryAddUpdateRequestModel> GetSubCategoryDetail(long SubCateogoryId)
        {
            return await (from c in _ctx.SubCategory
                          where c.Id == SubCateogoryId
                          select new SubCategoryAddUpdateRequestModel
                          {
                              Id = c.Id,
                              CategoryId = c.CategoryId,
                              Languages = c.SubCategoryLanguage.Select(x => new SubCategoryLanguageModel
                              {
                                  Language = x.Language,
                                  CategoryId = x.SubCategory.CategoryId,
                                  SubCategoryId = x.SubCategoryId,
                                  Name = x.Name,
                                  LanguageName = x.Language.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool IsNameExist(string Name, int CateogoryId, int SubCateogoryId)
        {
            bool IsExist = false;
            try
            {
                IsExist = (from x in _ctx.SubCategory
                           where x.Id != SubCateogoryId && x.CategoryId == CateogoryId && x.SubCategoryLanguage.Any(y => y.Name == Name)
                           select x
                           ).Any();
            }
            catch (Exception ex)
            {
                return IsExist;
            }

            return IsExist;
        }

        public async Task<ResponseModel<object>> Delete(long SubCategoryId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                SubCategory entity = await _ctx.SubCategory.FirstOrDefaultAsync(x => x.Id == SubCategoryId);
                if (entity != null)
                {
                    _ctx.SubCategory.Remove(entity);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Sub Category deleted successfully!";
                }
                else
                {
                    mResult.Message = "No sub Category found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = "This Sub category used in songs!";
            }

            return mResult;
        }

        public IQueryable<SubCategoryDropdownModel> GetSubCategoryDropdown(long CategoryId)
        {
            return (from c in _ctx.SubCategoryLanguage
                    where c.Language == GlobalConfig.defaultLanguageId
                    && c.SubCategory.CategoryId == CategoryId
                    select new SubCategoryDropdownModel
                    {
                        Id = c.SubCategoryId,
                        Name = c.Name
                    });
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
