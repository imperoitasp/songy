﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class RequestedSongRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        public RequestedSongRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<RequestedSongsAddUpdateModel> GetRequestedSongsDetail(long Id)
        {
            return await (from req in _ctx.UserRequestSongs
                          where req.Id == Id
                          select new RequestedSongsAddUpdateModel
                          {
                              Id = req.Id,
                              CategoryId = req.Songs.CategoryId,
                              imageName = req.Songs.ImageName,
                              FileName = req.FileName,
                              LanguageId = req.Songs.LanguageId,
                              Lyrics = req.Songs.Lyrics,
                              Title = req.Songs.Title,
                              SoongHashTag = req.HashTag.Select(x => new SongHashTagModel
                              {
                                  Id = x.Id,
                                  HashTag = x.Name

                              }).ToList()
                          }).FirstOrDefaultAsync();
        }
        public IQueryable<SongsViewModel> GetIQueryablePendingSongs()
        {
            IQueryable<SongsViewModel> objsongs;
            var query = (from req in _ctx.UserRequestSongs
                         where !req.IsUploaded
                         orderby req.CreatedDate descending
                         select new
                         {
                             Id = req.Id,
                             Title = req.Songs.Title,
                             CategoryId = req.Songs.CategoryId,
                             CategoryName = req.Songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == Language.English).CategoryName,
                             Lyrics = req.Lyrics,
                             LanguageId = req.Songs.LanguageId,
                             ImageName = req.Songs.ImageName,
                             FileName = req.FileName,
                             CreatedDate = req.CreatedDate,
                             TotalPurchase = req.UserPurchaseSongs.Count(),
                             HashTag = req.HashTag.Select(x => new SongHashTagModel
                             {
                                 Id = x.Id,
                                 HashTag = x.Name

                             }).ToList()
                         });

            if (query != null)
            {
                objsongs = query.Select(p => new SongsViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + p.FileName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    Title = p.Title,
                    CreatedDate = p.CreatedDate,
                    TotalPurchase = p.TotalPurchase,
                    HashTag = p.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag

                    }).ToList()
                });
                return objsongs;
            }
            return null;

        }

        public IQueryable<SongsViewModel> GetIQueryableUploadedSongs()
        {
            IQueryable<SongsViewModel> objsongs;
            var query = (from req in _ctx.UserRequestSongs
                         where req.IsUploaded
                         orderby req.CreatedDate descending
                         select new
                         {
                             Id = req.Id,
                             Title = req.Songs.Title,
                             CategoryId = req.Songs.CategoryId,
                             CategoryName = req.Songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == Language.English).CategoryName,
                             Lyrics = req.Lyrics,
                             LanguageId = req.Songs.LanguageId,
                             ImageName = req.Songs.ImageName,
                             FileName = req.FileName,
                             CreatedDate = req.CreatedDate,
                             TotalPurchase = req.UserPurchaseSongs.Count(),
                             IsUploaded = req.IsUploaded,
                             Price = req.UserPurchaseSongs.FirstOrDefault(x => x.RequestedSongsId == req.Id).Price,
                             HashTag = req.HashTag.Select(x => new SongHashTagModel
                             {
                                 Id = x.Id,
                                 HashTag = x.Name

                             }).ToList()
                         });

            if (query != null)
            {
                objsongs = query.Select(p => new SongsViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + p.FileName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    Title = p.Title,
                    CreatedDate = p.CreatedDate,
                    TotalPurchase = p.TotalPurchase,
                    Isuploaded = p.IsUploaded,
                    Price = p.Price,
                    HashTag = p.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag

                    }).ToList()
                });
                return objsongs;
            }
            return null;

        }

        public IQueryable<SongsViewModel> RequestedBySongs(long SongsId)
        {
            IQueryable<SongsViewModel> objsongs;
            var query = (from req in _ctx.UserRequestSongs
                         where req.SongsId == SongsId
                         orderby req.CreatedDate descending
                         select new
                         {
                             Id = req.Id,
                             Title = req.Songs.Title,
                             CategoryId = req.Songs.CategoryId,
                             CategoryName = req.Songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == Language.English).CategoryName,
                             Lyrics = req.Lyrics,
                             LanguageId = req.Songs.LanguageId,
                             ImageName = req.Songs.ImageName,
                             FileName = req.FileName,
                             CreatedDate = req.CreatedDate,
                             IsUploaded = req.IsUploaded,
                             TotalPurchase = req.UserPurchaseSongs.Count(),
                             Price = req.UserPurchaseSongs.FirstOrDefault(x => x.RequestedSongsId == req.Id).Price,
                             HashTag = req.HashTag.Select(x => new SongHashTagModel
                             {
                                 Id = x.Id,
                                 HashTag = x.Name

                             }).ToList()
                         });

            if (query != null)
            {
                objsongs = query.Select(p => new SongsViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + p.FileName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    Title = p.Title,
                    CreatedDate = p.CreatedDate,
                    Isuploaded = p.IsUploaded,
                    TotalPurchase = p.TotalPurchase,
                    Price = p.Price,
                    HashTag = p.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag

                    }).ToList()
                });
                return objsongs;
            }
            return null;

        }

        public IQueryable<UserDetailsModel> RequestedSongsDownloadByUser(long SongsId)
        {
            return (from req in _ctx.UserPurchaseSongs
                    where req.RequestedSongsId == SongsId
                    orderby req.PurchaseDate descending
                    select new UserDetailsModel
                    {
                        Id = req.UserId,
                        FullName = req.User.FirstName,
                        ProfileImageUrl = req.User.ProfileImageName,
                        Email = req.User.Email,
                        Title = req.UserRequestSongs.Songs.Title,
                        UserLanguage = req.User.Language,
                        CreatedDate = req.PurchaseDate
                    });

        }
        public async Task<ResponseModel<object>> InsertOrUpdateRequestedSongs(RequestedSongsAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await _ctx.UserRequestSongs.Include("Songs").Include("User").FirstOrDefaultAsync(x => x.Id == model.Id);
                //var data = await (from song in _ctx.UserRequestSongs
                //                  where song.Id == model.Id
                //                  select song).FirstOrDefaultAsync();
                if (data == null)
                {
                    isNew = true;
                    data = new UserRequestSongs();
                }
                if (!data.IsUploaded)
                {
                    var Message = string.Empty;
                    if (data.User.Language == Language.English)
                    {
                        Message = string.Format("<div style='font-family: Gotham-Book;font-size: 18px!important;'><font color='#fdba63'>{0}</font> is now available to download.</div>", data.Songs.Title);
                    }
                    else
                    {
                        Message = string.Format("<div align='right' style='font-family: Gotham-Book;font-size: 18px!important;'>.الآن جاهز للتنزيل <font color='#fdba63'>{0}</font></div>", data.Songs.Title);
                    }
                    await new NotificationRepository().SendNotification(NotificationType.SongAvaiForDownload, data.Id.ToString(), Message, data.UserId, model.UserId, data.SongsId);
                }
                data.FileName = model.FileName ?? data.FileName;
                data.Lyrics = model.Lyrics;
                data.IsUploaded = true;
                if (isNew)
                {

                    List<SongHashTag> soonghashtag = new List<SongHashTag>();
                    foreach (var item in model.SoongHashTag)
                    {
                        if (!string.IsNullOrWhiteSpace(item.HashTag))
                        {
                            soonghashtag.Add(new SongHashTag
                            {
                                Name = item.HashTag,
                                Id = data.Id
                            });
                        }
                    }
                    data.HashTag = soonghashtag;
                    _ctx.UserRequestSongs.Add(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Requested Song Added successfully";

                }
                else
                {
                    _ctx.SongHashTag.RemoveRange(_ctx.SongHashTag.Where(c => c.RequestSongsId == model.Id));
                    await _ctx.SaveChangesAsync();

                    List<SongHashTag> soonghashtag = new List<SongHashTag>();
                    foreach (var item in model.SoongHashTag)
                    {
                        if (!string.IsNullOrWhiteSpace(item.HashTag))
                        {
                            soonghashtag.Add(new SongHashTag
                            {
                                Name = item.HashTag,
                                RequestSongsId = data.Id
                            });
                        }
                    }

                    data.HashTag = soonghashtag;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Song update successfully";
                }
                await _ctx.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message + "InnerException" + ex.InnerException;
            }

            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
