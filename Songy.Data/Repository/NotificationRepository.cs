﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class NotificationRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;

        public NotificationRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public void Add(NotificationAddModel model)
        {
            try
            {
                UserNotifications data = new UserNotifications();
                data.CreatedDate = Utility.GetSystemDateTimeUTC();
                data.IsRead = false;
                data.NotificationType = model.NotificationTypeId;
                data.NotificationText = model.NotificationText;
                data.TablePrimaryID = model.TablePrimaryID;
                data.ReceiverID = model.ReceiverID;
                data.SenderID = model.SenderID;
                data.SongsID = model.SongID;
                data.ServerResponse = model.ServerResponse;
                _ctx.UserNotifications.Add(data);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ResponseModel<object>> GetNotificationList(NotificationGetModel model)
        {
            var MobUserRole = await _ctx.Roles.FirstOrDefaultAsync(p => p.Name == UserRoles.MobileUser.ToString());
            ResponseModel<object> mResult = new ResponseModel<object>();

            var query = await (from x in _ctx.UserNotifications
                               where x.ReceiverID == model.UserId
                               select new
                               {
                                   x.SenderID,
                                   x.Id,
                                   x.NotificationType,
                                   x.TablePrimaryID,
                                   x.NotificationText,
                                   x.CreatedDate,
                                   x.IsRead,
                                   x.ReceiverUser.FirstName,
                                   ImageName = (x.NotificationType != NotificationType.AdminNotification ? x.Songs.ImageName : GlobalConfig.SongsDefaultImage)
                               })
                               .OrderByDescending(SB => SB.Id)
                               .Skip(((model.PageIndex - 1) * model.PageSize))
                               .Take(model.PageSize)
                               .ToListAsync();
            if (query.Count > 0)
            {
                _ctx.UserNotifications
               .Where(x => x.ReceiverID == model.UserId).ToList()
               .ForEach(x =>
               {
                   x.IsRead = true;
               });
                await _ctx.SaveChangesAsync();
                var response = query.Select(x => new NotificationViewModel
                {
                    Name = x.FirstName,
                    IsRead = x.IsRead,
                    NotificationTypeId = x.NotificationType,
                    NotificationText = x.NotificationText,
                    ImageUrl = GlobalConfig.SongsImageUrl + x.ImageName,
                    TablePrimaryID = x.TablePrimaryID,
                    CreatedDateTimeUTC = x.CreatedDate,
                    NotificationID = x.Id,
                    TimeCaption = Utilities.TimeAgo(x.CreatedDate)
                }).ToList();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = string.Format("Count: {0}", response.Count());
                mResult.Result = response;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
                mResult.Result = new object[] { };
            }



            return mResult;
        }

        public async Task<ResponseModel<object>> ReadNotification(NotificationReadModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var notification = await _ctx.UserNotifications.FirstOrDefaultAsync(p => p.Id == model.NotificationId);
            if (notification != null)
            {
                notification.IsRead = true;
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;

            }
            return mResult;
        }

        public async Task<int> GetUnreadNotificationCount(UnreadNotificationGetModel model)
        {
            return await _ctx.UserNotifications.CountAsync(x => x.ReceiverID == model.ReceiverID && !x.IsRead);
        }

        public async Task SendNotification(NotificationType notificationType, string TablePrimaryID, string TablePrimaryText, string receiverUserId, string senderUserId, long? SongID)
        {
            var receiverUser = await _ctx.Users.Include(x => x.UserLoginTransactions).FirstOrDefaultAsync(p => p.Id == receiverUserId);

            string NotificationText = "";

            List<string> NotificationValues = new List<string>();

            string ServerResponse = "No such device registered.";

            switch (notificationType)
            {
                case NotificationType.NewSongsAdded:
                    NotificationText = TablePrimaryText;
                    break;
                case NotificationType.SongAvaiForDownload:
                    NotificationText = TablePrimaryText;
                    break;
                case NotificationType.AdminNotification:
                    NotificationText = TablePrimaryText;
                    break;
                default:
                    break;
            }

            var unreadCount = await GetUnreadNotificationCount(new UnreadNotificationGetModel() { ReceiverID = receiverUser.Id });

            //Push notification should not be sent to logged out users
            string[] DeviceTokenList = receiverUser.UserLoginTransactions.Where(x => !x.IsLogout).Select(x => x.DeviceToken).ToArray();

            NotificationPayload notificationSendModel = new NotificationPayload()
            {
                registration_ids = DeviceTokenList,
                data = new CustomDataModel()
                {
                    Name = receiverUser.FirstName,
                    NotificationType = notificationType,
                    Role = UserRoles.MobileUser,
                    TablePrimaryID = TablePrimaryID,
                },
                notification = new NotificationOptionModel()
                {
                    badge = unreadCount,
                    body = NotificationText,
                }
            };

            if (DeviceTokenList.Length > 0)
            {
                PushNotification pushNotification = new PushNotification();
                ServerResponse = pushNotification.SendNotification(notificationSendModel);
            }

            Add(new NotificationAddModel()
            {
                NotificationText = NotificationText,
                NotificationTypeId = notificationType,
                ReceiverID = receiverUser.Id,
                SenderID = senderUserId,
                ServerResponse = ServerResponse,
                TablePrimaryID = TablePrimaryID,
                SongID = SongID,
            });
        }

        public async Task<ResponseModel<object>> NotificationSend(NotificationSendModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            PushNotification pushNotification = new PushNotification();

            List<string> UserIdList = await (from a in _ctx.Users
                                             join b in _ctx.UserLoginTransaction on a.Id equals b.UserId
                                             where !b.IsLogout
                                             && (b.Platform == model.PlatformId || model.PlatformId == Platform.Web)
                                             select b.UserId).ToListAsync();
            UserIdList.Distinct();
            foreach (var UserId in UserIdList)
            {
                await new NotificationRepository()
              .SendNotification(NotificationType.AdminNotification, string.Empty, model.NotificationText, UserId, model.SenderID, model.SongId);
            }
            mResult.Status = ResponseStatus.Success;
            mResult.Message = "Sent notifications successfully.";
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
