﻿using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class ApplicationRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public ApplicationRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public ApplicationSettingModel GetAppSettings()
        {
            return (from setting in _ctx.ApplicationSetting
                    select new ApplicationSettingModel
                    {
                        ForceUpdate = setting.ForceUpdate,
                        Maintenance = setting.Maintenance,
                        AndroidVersion = setting.AndroidVersion,
                        iOSVersion = setting.IOSVersion
                    }).FirstOrDefault();
        }

        public async Task<ApplicationSettingModel> GetAppSettingAsync()
        {
            return await (from setting in _ctx.ApplicationSetting
                          select new ApplicationSettingModel
                          {
                              ForceUpdate = setting.ForceUpdate,
                              Maintenance = setting.Maintenance,
                              AndroidVersion = setting.AndroidVersion,
                              iOSVersion = setting.IOSVersion
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> UpdateSettings(ApplicationSettingModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ApplicationSetting data = await _ctx.ApplicationSetting.FirstOrDefaultAsync();
                bool isNew = false;
                if (data == null)
                {
                    data = new ApplicationSetting();
                    isNew = true;
                }
                data.IOSVersion = model.iOSVersion;
                data.AndroidVersion = model.AndroidVersion;
                data.ForceUpdate = model.ForceUpdate;
                data.Maintenance = model.Maintenance;
                if (isNew)
                    _ctx.ApplicationSetting.Add(data);
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Application settings saved successfully!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
