﻿using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class UtilityRepository
    {
        private ApplicationDbContext _ctx;

        public UtilityRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public UtilityModel Term()
        {
            return (from p in _ctx.TermsSongy
                    select new UtilityModel
                    {
                        Id = p.Id,
                        DescriptionEnglish = p.TermsEnglish,
                        DescriptionArbic = p.TermsArbic
                    }).FirstOrDefault();

        }

        public async Task<int> UpdateTermsConditions(UtilityModel model)
        {
            var objUpdate = _ctx.TermsSongy.FirstOrDefault();
            bool isNew = false;
            if (objUpdate == null)
            {
                isNew = true;
                objUpdate = new TermsSongy();
            }
            objUpdate.TermsEnglish = model.DescriptionEnglish;
            objUpdate.TermsArbic = model.DescriptionArbic;
            if (isNew)
                _ctx.TermsSongy.Add(objUpdate);
            return await _ctx.SaveChangesAsync();
        }

        public UtilityModel About()
        {
            return (from p in _ctx.AboutSongy
                    select new UtilityModel
                    {
                        Id = p.Id,
                        DescriptionEnglish = p.AboutEnglish,
                        DescriptionArbic = p.AboutArbic
                    }).FirstOrDefault();

        }

        public async Task<int> UpdateAbout(UtilityModel model)
        {
            var objUpdate = _ctx.AboutSongy.FirstOrDefault();
            bool isNew = false;
            if (objUpdate == null)
            {
                isNew = true;
                objUpdate = new AboutSongy();
            }
            objUpdate.AboutArbic = model.DescriptionArbic;
            objUpdate.AboutEnglish = model.DescriptionEnglish;
            if (isNew)
                _ctx.AboutSongy.Add(objUpdate);
            return await _ctx.SaveChangesAsync();
        }

        public async Task<ResponseModel<object>> ContactUs(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                ContactUs data = new ContactUs
                {
                    UserId = model.UserId,
                    Message = model.Message,
                };
                _ctx.ContactUs.Add(data);
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.ContactUsSuccess;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> GetAboutUs(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == UserId);

            var objAbout = await (from a in _ctx.AboutSongy
                                  select new AboutModel
                                  {
                                      Id = a.Id,
                                      AboutUs = loggedInUser.Language == Language.English ? a.AboutEnglish : a.AboutArbic
                                  }).FirstOrDefaultAsync();
            mResult.Result = objAbout.AboutUs;
            mResult.Status = ResponseStatus.Success;
            return mResult;
        }

        public async Task<ResponseModel<object>> GetTerms(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == UserId);

            var objTerms = await (from a in _ctx.TermsSongy
                                  select new TermModel
                                  {
                                      Id = a.Id,
                                      Terms = loggedInUser.Language == Language.English ? a.TermsEnglish : a.TermsArbic
                                  }).FirstOrDefaultAsync();
            mResult.Result = objTerms.Terms;
            mResult.Status = ResponseStatus.Success;
            return mResult;
        }
        public async Task<AdminDashboardModel> GetAddminDashboard()
        {
            var userList = await _ctx.Users.Where(x => x.UserRole == UserRoles.MobileUser).CountAsync();
            var requested = await _ctx.UserRequestSongs.ToListAsync();
            return new AdminDashboardModel()
            {
                TotalUser = userList,
                TotalPurchase = _ctx.UserPurchaseSongs.Count(),
                TotalRequest = requested.Count(),
                TotalSongs = _ctx.Songs.Count(),
                TotalDownload = _ctx.UserSongsDownload.Count(),
                TotalUpload = requested.Count(x => x.IsUploaded),
                TotalPending = requested.Count(x => !x.IsUploaded),
                TotalInquiry = _ctx.ContactUs.Count(),
            };
        }
    }
}
