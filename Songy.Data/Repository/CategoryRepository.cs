﻿using LinqKit;
using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class CategoryRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public CategoryRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<ResponseModel<object>> AddUpdate(AdminCategoryModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                Category entity = await _ctx.Category.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (entity == null)
                {
                    mResult.Message = "Category record added successfully";
                    entity = new Category();
                }
                else
                {
                    mResult.Message = "Category record updated successfully";
                }
                int MaxOrderId = 0;
                if (_ctx.Category.Any())
                    MaxOrderId = _ctx.Category.Max(x => x.Order);

                entity.Order = MaxOrderId + 1;
                entity.ImageName = model.ImageName ?? entity.ImageName;
                entity.IconName = model.IconName ?? entity.IconName;
                entity.IsSubCateogory = model.IsSubCateogory;
                entity.PriceIdAndroid = model.PriceIdAndroid;
                entity.PriceIdiOS = model.PriceIdiOS;

                if (entity.CategoryLanguage == null)
                {
                    entity.CategoryLanguage = new List<CategoryLanguage>();
                }

                if (entity.CategoryLanguage.Any())
                {
                    _ctx.CategoryLanguage.RemoveRange(entity.CategoryLanguage);
                }
                if (model.Categorylanguagemodellist != null)
                {
                    foreach (CategoryLanguageModel item in model.Categorylanguagemodellist)
                    {
                        entity.CategoryLanguage.Add(new CategoryLanguage() { CategoryName = item.CategoryName, Language = item.Language });
                    }
                }
                if (entity.Id == 0)
                {
                    _ctx.Category.Add(entity);
                }
                try
                {
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
                catch (DbEntityValidationException err)
                {
                    foreach (DbEntityValidationResult validationErrors in err.EntityValidationErrors)
                    {
                        foreach (DbValidationError validationError in validationErrors.ValidationErrors)
                        {
                            if (!string.IsNullOrWhiteSpace(mResult.Message))
                            {
                                mResult.Message += Environment.NewLine;
                            }
                            mResult.Message += $"Property: {validationError.PropertyName}, Error: {validationError.ErrorMessage}";
                        }
                    }
                }
                catch (Exception err)
                {
                    mResult.Message = err.Message;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public IQueryable<AdminCategoryModel> GetAllCategory_IQueryable()
        {
            return (from c in _ctx.Category
                    select new AdminCategoryModel
                    {
                        Id = c.Id,
                        Order = c.Order,
                        ImageName = c.ImageName,
                        IconName = c.IconName,
                        IsSubCateogory=c.IsSubCateogory,
                        PriceIdAndroid=c.PriceIdAndroid,
                        PriceIdiOS=c.PriceIdiOS,
                        Categorylanguagemodellist = c.CategoryLanguage.Select(x => new CategoryLanguageModel
                        {
                            Language = x.Language,
                            LanguageName = x.Language.ToString(),
                            CategoryName = x.CategoryName
                        }).ToList()
                    }).OrderBy(x => x.Order);
        }

        public IQueryable<CategoryDropdownModel> GetCategoryDropdown(bool FromSong)
        {

            var predicate = PredicateBuilder.True<CategoryLanguage>();

            if (FromSong==false)
            {
                predicate = predicate.And(x=>x.Category.IsSubCateogory==true);
            }
            return (from c in _ctx.CategoryLanguage.Where(predicate)
                    where c.Language == GlobalConfig.defaultLanguageId
                    
                    select new CategoryDropdownModel
                    {
                        CategoryId = c.CategoryId,
                        CategoryName = c.CategoryName
                    });
        }

        public bool isCategoryDuplicate(AdminCategoryModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.Categorylanguagemodellist)
            {
                isDuplicate = _ctx.CategoryLanguage.Any(x => x.Language == item.Language && x.CategoryName == item.CategoryName && x.CategoryId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<ResponseModel<object>> GetCategory(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == UserId);
            if (user == null)
            {
                mResult.Message = ResponseMessages.UserNotFound;
                return mResult;
            }

            var Category = (from c in _ctx.CategoryLanguage
                            where c.Language == user.Language
                            select new CategoryViewModel
                            {
                                Id = c.CategoryId,
                                Name = c.CategoryName,
                                IconUrl = GlobalConfig.CategoryIconUrl + c.Category.IconName,
                                ImageUrl = GlobalConfig.CategoryImageUrl + c.Category.ImageName,
                                SortOrder = c.Category.Order
                            }).OrderBy(x => x.SortOrder).ToList();

            if (Category.Count > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Result = Category;
            }
            else
            {
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Delete(long CategoryId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                Category entity = await _ctx.Category.FirstOrDefaultAsync(x => x.Id == CategoryId);
                if (entity != null)
                {
                    _ctx.Category.Remove(entity);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Category deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Category found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = "This category used in songs!";
            }

            return mResult;
        }

        public async Task<AdminCategoryModel> GetCategoryDetail(long CatrgoryId)
        {
            return await (from c in _ctx.Category
                          where c.Id == CatrgoryId
                          select new AdminCategoryModel
                          {
                              Id = c.Id,
                              ImageName = c.ImageName,
                              IconName = c.IconName,
                              IsSubCateogory=c.IsSubCateogory,
                              PriceIdAndroid=c.PriceIdAndroid,
                              PriceIdiOS=c.PriceIdiOS,
                              Categorylanguagemodellist = c.CategoryLanguage.Select(x => new CategoryLanguageModel
                              {
                                  Language = x.Language,
                                  CategoryId=x.CategoryId,
                                  CategoryName = x.CategoryName,
                                  LanguageName = x.Language.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool IsTitleExist(string CategoryName, long CategoryId)
        {
            bool IsExist = false;
            try
            {
                IsExist = (from x in _ctx.Category
                           where x.Id != CategoryId && x.CategoryLanguage.Any(y => y.CategoryName == CategoryName)
                           select x
                           ).Any();
            }
            catch (Exception ex)
            {
                return IsExist;
            }

            return IsExist;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }

    }
}
