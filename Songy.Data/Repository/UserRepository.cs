﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Data.Entities;
using Songy.Data.Helper;
using Songy.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Songy.Data.Repository
{
    public class UserRepository: IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IOwinContext _owin;

        public UserRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
        }

        public bool CheckUserIsActive(string UserId)
        {
            return _ctx.Users.Any(x => x.Id == UserId) ? _ctx.Users.FirstOrDefault(x => x.Id == UserId).IsActive : false;
        }

        public async Task<string> FindUser(string UserName, string Password)
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(UserName);
            if (user == null)
            {
                user = await _userManager.FindByNameAsync(UserName);
            }

            if (user == null)
            {
                user = await _userManager.FindAsync(UserName, Password);
            }

            return user != null ? user.Id : string.Empty;
        }

        public bool IsValidUser(string NameIdentifier)
        {
            ApplicationUser user = _userManager.FindById(NameIdentifier);
            bool isValid = user != null;
            return isValid;
        }

        public UserRepository(IOwinContext owin)
        {
            _owin = owin;
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx))
            {
                EmailService = new EmailService()
            };
            _authenticationManager = owin.Authentication;
        }

        public async Task<ResponseModel<object>> ExternalLogin(ExternalConnectModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            UserLoginInfo userLoginInfo = new UserLoginInfo(model.ExternalConnectType.ToString(), model.ExternalConnectId);
            LoginResponseModel objResponse = new LoginResponseModel
            {
                AccessToken = { },
                UserDetail = { }
            };

            ApplicationUser data = null;
            data = await _userManager.FindAsync(userLoginInfo);
            if (data == null)
            {
                if (!string.IsNullOrEmpty(model.EmailID))
                {
                    data = await _userManager.FindByEmailAsync(model.EmailID);
                }
                if (data == null)
                {
                    data = new ApplicationUser
                    {
                        Email = model.EmailID,
                        FirstName = model.FullName,
                        UserName = model.ExternalConnectId,
                        IsActive = true,
                        UserRole = UserRoles.MobileUser,
                    };
                    IdentityResult result = new IdentityResult();

                    try
                    {
                        result = await _userManager.CreateAsync(data, model.ExternalConnectId);
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                        {
                            foreach (DbValidationError ve in eve.ValidationErrors)
                            {
                                mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        return mResult;
                    }
                    catch (Exception ex)
                    {
                        mResult.Message = ex.Message;
                        return mResult;
                    }
                    if (result.Succeeded)
                    {
                        await _userManager.AddLoginAsync(data.Id, userLoginInfo);
                        //Add Role
                        IdentityResult roleResult = await _userManager.AddToRoleAsync(data.Id, UserRoles.MobileUser.ToString());
                        if (roleResult.Succeeded)
                        {
                            //Add User Device Configure
                            UserLogin(data.Id, model.DeviceToken, model.Platform);

                            ResponseModel<AccessTokenModel> mTokenResult = await GetUserToken(data.UserName, model.ExternalConnectId);
                            if (mTokenResult != null)
                            {
                                if (mTokenResult.Status == ResponseStatus.Success)
                                {
                                    objResponse.AccessToken = new AccessTokenModel
                                    {
                                        AccessToken = mTokenResult.Result.AccessToken,
                                        Error = mTokenResult.Result.Error,
                                        ExpiresIn = mTokenResult.Result.ExpiresIn,
                                        RefreshToken = mTokenResult.Result.RefreshToken,
                                        TokenType = mTokenResult.Result.TokenType,
                                    };

                                    objResponse.UserDetail = new UserDetailModel
                                    {
                                        FullName = data.FirstName,
                                        UserName = data.UserName,
                                        Email = data.Email,
                                        UserRole = data.UserRole,
                                        ProfileImageUrl = !string.IsNullOrWhiteSpace(data.ProfileImageName) ? GlobalConfig.UserImageUrl + data.ProfileImageName : string.Empty,
                                        UserLanguage = data.Language,
                                        ExternalConnectType = model.ExternalConnectType
                                    };

                                    mResult.Result = objResponse;
                                    mResult.Status = ResponseStatus.Success;
                                    mResult.Message = $"{ResponseMessages.WelcomeRegister} { data.FirstName} - { GlobalConfig.ProjectName}";
                                }
                                else
                                {
                                    mResult.Message = mTokenResult.Message;
                                }
                            }
                        }
                        else
                        {
                            mResult.Message = string.Join("; ", result.Errors);
                        }
                    }
                    else
                    {
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
                else
                {
                    data.Email = model.EmailID;
                    data.FirstName = model.FullName;
                    await _ctx.SaveChangesAsync();
                    bool isValidRole = await _userManager.IsInRoleAsync(data.Id, UserRoles.MobileUser.ToString());
                    if (isValidRole)
                    {
                        await _userManager.AddLoginAsync(data.Id, userLoginInfo);
                        //Add User Device Configure
                        UserLogin(data.Id, model.DeviceToken, model.Platform);

                        ResponseModel<AccessTokenModel> mTokenResult = await GetUserToken(data.UserName, model.ExternalConnectId);
                        if (mTokenResult != null)
                        {
                            if (mTokenResult.Status == ResponseStatus.Success)
                            {
                                objResponse.AccessToken = new AccessTokenModel
                                {
                                    AccessToken = mTokenResult.Result.AccessToken,
                                    Error = mTokenResult.Result.Error,
                                    ExpiresIn = mTokenResult.Result.ExpiresIn,
                                    RefreshToken = mTokenResult.Result.RefreshToken,
                                    TokenType = mTokenResult.Result.TokenType,
                                };

                                objResponse.UserDetail = new UserDetailModel
                                {
                                    FullName = data.FirstName,
                                    UserName = data.UserName,
                                    Email = data.Email,
                                    UserRole = data.UserRole,
                                    ProfileImageUrl = !string.IsNullOrWhiteSpace(data.ProfileImageName) ? GlobalConfig.UserImageUrl + data.ProfileImageName : string.Empty,
                                    UserLanguage = data.Language,
                                    ExternalConnectType = model.ExternalConnectType
                                };

                                mResult.Result = objResponse;
                                mResult.Status = ResponseStatus.Success;
                                mResult.Message = mResult.Message = $"{ResponseMessages.LoggedInSuccess} - { GlobalConfig.ProjectName}";
                            }
                            else
                            {
                                mResult.Message = mTokenResult.Message;
                            }
                        }
                        else
                        {
                            mResult.Message = mTokenResult.Message;
                        }
                    }
                }
            }
            else
            {
                if (data.IsActive)
                {
                    data.Email = model.EmailID;
                    data.FirstName = model.FullName;
                    bool isValidRole = await _userManager.IsInRoleAsync(data.Id, UserRoles.MobileUser.ToString());
                    if (isValidRole)
                    {
                        UserLogin(data.Id, model.DeviceToken, model.Platform);

                        ResponseModel<AccessTokenModel> mTokenResult = await GetUserToken(data.UserName, model.ExternalConnectId);
                        if (mTokenResult != null)
                        {
                            if (mTokenResult.Status == ResponseStatus.Success)
                            {
                                objResponse.AccessToken = new AccessTokenModel
                                {
                                    AccessToken = mTokenResult.Result.AccessToken,
                                    Error = mTokenResult.Result.Error,
                                    ExpiresIn = mTokenResult.Result.ExpiresIn,
                                    RefreshToken = mTokenResult.Result.RefreshToken,
                                    TokenType = mTokenResult.Result.TokenType,
                                };

                                objResponse.UserDetail = new UserDetailModel
                                {
                                    FullName = data.FirstName,
                                    UserName = data.UserName,
                                    Email = data.Email,
                                    UserRole = data.UserRole,
                                    ProfileImageUrl = !string.IsNullOrWhiteSpace(data.ProfileImageName) ? GlobalConfig.UserImageUrl + data.ProfileImageName : string.Empty,
                                    UserLanguage = data.Language,
                                    ExternalConnectType = model.ExternalConnectType
                                };

                                mResult.Result = objResponse;
                                mResult.Status = ResponseStatus.Success;
                                mResult.Message = mResult.Message = $"{ResponseMessages.LoggedInSuccess} - { GlobalConfig.ProjectName}";
                            }
                            else
                            {
                                mResult.Status = ResponseStatus.Failed;
                                mResult.Message = mTokenResult.Message;
                            }
                        }
                        else
                        {
                            mResult.Message = mTokenResult.Message;
                        }
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.NotFound;
                        mResult.Message = ResponseMessages.InvalidRole;
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.AdminBlocked;
                    mResult.Message = ResponseMessages.AdminBlocked;
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> UserLoginWithEmail(UserSignInModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            LoginResponseModel objResponse = new LoginResponseModel
            {
                AccessToken = { },
                UserDetail = { }
            };
            ApplicationUser data = await _userManager.FindByNameAsync(model.EmailID);
            if (data != null)
            {
                if (data.IsActive)
                {
                    data.Language = model.Language;
                    await _userManager.UpdateAsync(data);
                    bool validPassword = await _userManager.CheckPasswordAsync(data, model.Password);
                    if (validPassword)
                    {
                        ResponseModel<AccessTokenModel> mTokenResult = await GetUserToken(model.EmailID, model.Password);
                        if (mTokenResult != null)
                        {
                            if (mTokenResult.Status == ResponseStatus.Success)
                            {
                                //Add User Device Configure
                                UserLogin(data.Id, model.DeviceToken, model.Platform);

                                objResponse.AccessToken = new AccessTokenModel
                                {
                                    AccessToken = mTokenResult.Result.AccessToken,
                                    Error = mTokenResult.Result.Error,
                                    ExpiresIn = mTokenResult.Result.ExpiresIn,
                                    RefreshToken = mTokenResult.Result.RefreshToken,
                                    TokenType = mTokenResult.Result.TokenType,
                                };

                                objResponse.UserDetail = new UserDetailModel
                                {
                                    FullName = data.FirstName,
                                    UserName = data.UserName,
                                    Email = data.Email,
                                    UserRole = data.UserRole,
                                    ProfileImageUrl = !string.IsNullOrWhiteSpace(data.ProfileImageName) ? GlobalConfig.UserImageUrl + data.ProfileImageName : string.Empty,
                                    UserLanguage = data.Language,
                                    ExternalConnectType = model.ExternalConnectType
                                };

                                mResult.Result = objResponse;
                                mResult.Status = ResponseStatus.Success;
                                mResult.Message = mResult.Message = $"{ResponseMessages.LoggedInSuccess} - { GlobalConfig.ProjectName}";
                            }
                            else
                            {
                                mResult.Message = mTokenResult.Message;
                            }
                        }
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = ResponseMessages.NoUserRegisterForEmail;
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.AdminBlocked;
                    mResult.Message = ResponseMessages.AdminBlocked;
                }

            }
            else
            {
                mResult.Status = ResponseStatus.UserNotExist;
                mResult.Message = ResponseMessages.UserNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> UserSighnUp(UserSignUpModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            LoginResponseModel objResponse = new LoginResponseModel
            {
                AccessToken = { },
                UserDetail = { }
            };

            ApplicationUser data = null;
            data = await _userManager.FindByEmailAsync(model.EmailID);
            if (data != null)
            {
                mResult.Message = ResponseMessages.AlredyRegister;
                return mResult;
            }
            data = await _userManager.FindByNameAsync(model.EmailID);
            if (data != null)
            {
                mResult.Message = ResponseMessages.AlredyRegister;
            }
            else
            {
                data = new ApplicationUser
                {
                    Email = model.EmailID,
                    UserName = model.EmailID,
                    FirstName = model.FullName,
                    Language = model.Language,
                    UserRole = UserRoles.MobileUser,
                    ExternalConnectType = model.ExternalConnectType
                };
                IdentityResult result = new IdentityResult();
                try
                {
                    result = await _userManager.CreateAsync(data, model.Password);
                }
                catch (DbEntityValidationException e)
                {
                    foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                    {
                        foreach (DbValidationError ve in eve.ValidationErrors)
                        {
                            mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return mResult;
                }
                catch (Exception ex)
                {
                    mResult.Message = ex.Message;
                    return mResult;
                }
                if (result.Succeeded)
                {
                    //Add Role
                    IdentityResult roleResult = await _userManager.AddToRoleAsync(data.Id, UserRoles.MobileUser.ToString());
                    if (roleResult.Succeeded)
                    {
                        //Add User Device Configure
                        UserLogin(data.Id, model.DeviceToken, model.Platform);

                        ResponseModel<AccessTokenModel> mTokenResult = await GetUserToken(data.UserName, model.Password);
                        if (mTokenResult != null)
                        {
                            if (mTokenResult.Status == ResponseStatus.Success)
                            {
                                objResponse.AccessToken = new AccessTokenModel
                                {
                                    AccessToken = mTokenResult.Result.AccessToken,
                                    Error = mTokenResult.Result.Error,
                                    ExpiresIn = mTokenResult.Result.ExpiresIn,
                                    RefreshToken = mTokenResult.Result.RefreshToken,
                                    TokenType = mTokenResult.Result.TokenType,
                                };

                                objResponse.UserDetail = new UserDetailModel
                                {
                                    FullName = data.FirstName,
                                    UserName = data.UserName,
                                    Email = data.Email,
                                    UserRole = data.UserRole,
                                    ProfileImageUrl = !string.IsNullOrWhiteSpace(data.ProfileImageName) ? GlobalConfig.UserImageUrl + data.ProfileImageName : string.Empty,
                                    UserLanguage = data.Language,
                                    ExternalConnectType = model.ExternalConnectType
                                };

                                mResult.Result = objResponse;
                                mResult.Status = ResponseStatus.Success;
                                mResult.Message = $"{ResponseMessages.WelcomeRegister} { data.FirstName} - { GlobalConfig.ProjectName}";
                            }
                            else
                            {
                                mResult.Message = mTokenResult.Message;
                            }
                        }
                    }
                    else
                    {
                        mResult.Message = string.Join("; ", roleResult.Errors);
                    }
                }
                else
                {
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> GetMyProfileForEdit(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var profileUser = await (from user in _ctx.Users
                                     where user.Id == UserId
                                     select new
                                     {
                                         user.Email,
                                         user.FirstName,
                                         user.PhoneNumber,
                                         user.UserName,
                                         user.ProfileImageName
                                     }).FirstOrDefaultAsync();
            if (profileUser != null)
            {
                MyProfileResponseModel response = new MyProfileResponseModel()
                {
                    Email = profileUser.Email,
                    FullName = profileUser.FirstName,
                    Username = profileUser.UserName,
                    ProfileImageUrl = !string.IsNullOrWhiteSpace(profileUser.ProfileImageName) ? GlobalConfig.UserImageUrl + profileUser.ProfileImageName : string.Empty
                };
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "";
                mResult.Result = response;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SaveMyProfile(EditMyProfileSumbitModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser profileUser = await (from user in _ctx.Users
                                                 where user.Id == model.UserId
                                                 select user)
                            .SingleOrDefaultAsync();

            if (profileUser != null)
            {
                profileUser.FirstName = model.FullName;
                await _ctx.SaveChangesAsync();

                UserDetailModel UserData = new UserDetailModel()
                {
                    FullName = profileUser.FirstName,
                    UserName = profileUser.UserName,
                    Email = profileUser.Email,
                    ProfileImageUrl = !string.IsNullOrWhiteSpace(profileUser.ProfileImageName) ? GlobalConfig.UserImageUrl + profileUser.ProfileImageName : string.Empty,
                    UserLanguage = profileUser.Language,
                };

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.ProfileUpdated;
                mResult.Result = UserData;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SaveMyProfileLanguage(SaveMyProfileLanguageSumbitModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser profileUser = await (from user in _ctx.Users
                                                 where user.Id == model.UserId
                                                 select user)
                            .SingleOrDefaultAsync();

            if (profileUser != null)
            {
                profileUser.Language = model.Language;
                await _ctx.SaveChangesAsync();

                UserDetailModel UserData = new UserDetailModel()
                {
                    FullName = profileUser.FirstName,
                    UserName = profileUser.UserName,
                    Email = profileUser.Email,
                    ProfileImageUrl = !string.IsNullOrWhiteSpace(profileUser.ProfileImageName) ? GlobalConfig.UserImageUrl + profileUser.ProfileImageName : string.Empty,
                    UserLanguage = profileUser.Language,
                };

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.ProfileUpdated;
                mResult.Result = UserData;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SaveMyProfileImage(string ProfileImageName, string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser profileUser = await (from user in _ctx.Users
                                                 where user.Id == UserId
                                                 select user)
                            .SingleOrDefaultAsync();

            if (profileUser != null)
            {
                profileUser.ProfileImageName = ProfileImageName;

                await _ctx.SaveChangesAsync();
                UserDetailModel UserData = new UserDetailModel()
                {
                    FullName = profileUser.FirstName,
                    UserName = profileUser.UserName,
                    Email = profileUser.Email,
                    ProfileImageUrl = !string.IsNullOrWhiteSpace(profileUser.ProfileImageName) ? GlobalConfig.UserImageUrl + profileUser.ProfileImageName : string.Empty,
                    UserLanguage = profileUser.Language,
                };
                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.ProfileUpdated;
                mResult.Result = UserData;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ChangePassword(UserChangePasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = ResponseMessages.UnauthorisedAccess;
            }
            else
            {
                bool validPassword = await _userManager.CheckPasswordAsync(user, model.CurrentPassword);
                if (validPassword)
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = ResponseMessages.PasswordChanged;
                    }
                    else
                    {
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = ResponseMessages.CurrentPasswordWrong;

                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser user = await _userManager.FindByNameAsync(model.EmailID);
            if (user != null)
            {
                string PasswordResetToken = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                string encodeToken = HttpUtility.UrlEncode(PasswordResetToken);

                string resetlink = string.Format("{0}Admin/Account/ResetPassword?passwordResetToken={1}&emailID={2}", GlobalConfig.WebBaseUrl, encodeToken, user.Email);

                Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>
                {
                    { "{{name}}", user.FirstName },
                    { "{{url}}", GlobalConfig.EmailTemplateUrl },
                    { "{{link}}", resetlink }
                };

                IdentityMessage message = new IdentityMessage
                {
                    Subject = string.Format("Reset Password : {0}", GlobalConfig.ProjectName),
                    Body = Helper.Utility.GenerateEmailBody(EmailTemplate.ForgotPassword, dicPlaceholders),
                    Destination = user.Email
                };
                await new EmailService().SendAsync(message);

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.PasswordResetEmailSent;
            }
            else
            {
                mResult.Message = ResponseMessages.NoUserRegisterForEmail;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ForgotPasswordByAdmin(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser user = await _userManager.FindByIdAsync(UserId);
            if (user != null)
            {
                string PasswordResetToken = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                string encodeToken = HttpUtility.UrlEncode(PasswordResetToken);

                string resetlink = string.Format("{0}Admin/Account/ResetPassword?passwordResetToken={1}&emailID={2}", GlobalConfig.WebBaseUrl, encodeToken, user.Email);

                Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>
                {
                    { "{{name}}", user.FirstName },
                    { "{{url}}", GlobalConfig.EmailTemplateUrl },
                    { "{{link}}", resetlink }
                };

                IdentityMessage message = new IdentityMessage
                {
                    Subject = string.Format("Reset Password : {0}", GlobalConfig.ProjectName),
                    Body = Helper.Utility.GenerateEmailBody(EmailTemplate.ForgotPassword, dicPlaceholders),
                    Destination = user.Email
                };
                await new EmailService().SendAsync(message);

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.PasswordResetEmailSent;
            }
            else
            {
                mResult.Message = ResponseMessages.NoUserRegisterForEmail;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ResetPassword(ResetPasswordModel model, bool isSendEmail = true)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Failed
            };
            ApplicationUser user = await _userManager.FindByEmailAsync(model.EmailID);

            if (user != null)
            {
                IdentityResult result = await _userManager.ResetPasswordAsync(user.Id, model.PasswordResetToken, model.NewPassword);
                _userManager.UpdateSecurityStamp(user.Id);
                if (result.Succeeded)
                {
                    if (isSendEmail)
                    {
                        StringBuilder mailContent = new StringBuilder();
                        mailContent.Append("Hello " + user.FirstName + ",");
                        mailContent.Append("<BR/><BR/>");
                        mailContent.Append("Greetings from " + GlobalConfig.ProjectName + " as per your request, we have successfully changed your password.");
                        mailContent.Append("<BR/><BR/><BR/>");
                        mailContent.Append("Thanks");
                        mailContent.Append("<BR/>");
                        mailContent.Append(GlobalConfig.ProjectName + " Team!");

                        IdentityMessage message = new IdentityMessage
                        {
                            Subject = string.Format("{0}: Password reset", GlobalConfig.ProjectName),
                            Body = mailContent.ToString(),
                            Destination = user.Email
                        };
                        await new EmailService().SendAsync(message);
                        await _userManager.SendEmailAsync(user.Id, message.Subject, message.Body);
                    }

                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = ResponseMessages.PasswordReseted;
                }
                else
                {
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<ApplicationUser>> ForgotPassword(string userName, string password)
        {
            ResponseModel<ApplicationUser> mResult = new ResponseModel<ApplicationUser>();
            ApplicationUser user = await _userManager.FindByEmailAsync(userName);
            mResult = new ResponseModel<ApplicationUser>();

            if (user != null)
            {
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(password);
                IdentityResult result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    mResult.Status = ResponseStatus.Success;
                }
                else
                {
                    mResult.Status = ResponseStatus.NotFound;
                    mResult.Message = result.Errors.ToString();
                    return mResult;
                }
            }
            else
            {
                mResult.Message = ResponseMessages.UserNotFound;
            }

            return mResult;
        }

        public UserDetailModel GetUserData(string UserID)
        {
            ApplicationUser user = _ctx.Users.FirstOrDefault(x => x.Id == UserID);
            UserDetailModel UserData;
            if (user != null)
            {
                IList<string> role = _userManager.GetRoles(user.Id);
                string roleName = role.FirstOrDefault();
                UserData = new UserDetailModel()
                {
                    FullName = user.FirstName,
                    UserName = user.UserName,
                    Email = user.Email,
                    UserRole = (UserRoles)Enum.Parse(typeof(UserRoles), roleName),
                    ProfileImageUrl = !string.IsNullOrWhiteSpace(user.ProfileImageName) ? GlobalConfig.UserImageUrl + user.ProfileImageName : string.Empty,
                    UserLanguage = user.Language,
                };
            }
            else
            {
                UserData = new UserDetailModel();
            }
            return UserData;
        }

        private void UserLogin(string UserID, string DeviceToken, Platform Platform)
        {
            UserLoginTransaction data = _ctx.UserLoginTransaction.FirstOrDefault(x => x.DeviceToken == DeviceToken);
            if (data == null)
            {
                data = new UserLoginTransaction()
                {
                    LoginDateTime = Utility.GetSystemDateTimeUTC(),
                    DeviceToken = DeviceToken,
                    Platform = Platform,
                    UserId = UserID,
                };
                _ctx.UserLoginTransaction.Add(data);
            }
            else
            {
                data.IsLogout = false;
                data.UserId = UserID;
                data.Platform = Platform;
                data.LoginDateTime = Utility.GetSystemDateTimeUTC();
            }
            try
            {
                _ctx.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<ResponseModel<object>> Logout(UserSignoutModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                UserLogout(model.DeviceToken);
                ApplicationUser user = await _userManager.FindByIdAsync(model.UserId);
                if (user != null)
                {

                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = ResponseMessages.Logout;

                }
                else
                {
                    mResult.Status = ResponseStatus.Unauthorized;
                    mResult.Message = ResponseMessages.UnauthorisedAccess;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        private void UserLogout(string DeviceToken)
        {
            UserLoginTransaction data = _ctx.UserLoginTransaction.Where(x => x.DeviceToken == DeviceToken).FirstOrDefault();
            if (data != null)
            {
                data.IsLogout = true;
                data.LogoutDateTime = Utility.GetSystemDateTimeUTC();
                _ctx.SaveChanges();
            }
        }

        private async Task<ResponseModel<AccessTokenModel>> GetUserToken(string UserName, string Password)
        {
            ResponseModel<AccessTokenModel> mResult = new ResponseModel<AccessTokenModel>();
            using (HttpClient client = new HttpClient())
            {
                List<KeyValuePair<string, string>> requestParams = new List<KeyValuePair<string, string>>
                            {
                                new KeyValuePair<string, string>("grant_type", "password"),
                                new KeyValuePair<string, string>("username", UserName),
                                new KeyValuePair<string, string>("password", Password),
                            };
                HttpRequest request = HttpContext.Current.Request;
                string tokenServiceUrl = request.Url.GetLeftPart(UriPartial.Authority) + request.ApplicationPath + "/Token";
                FormUrlEncodedContent requestParamsFormUrlEncoded = new FormUrlEncodedContent(requestParams);
                HttpResponseMessage tokenServiceResponse = await client.PostAsync(tokenServiceUrl, requestParamsFormUrlEncoded);
                string responseString = await tokenServiceResponse.Content.ReadAsStringAsync();
                HttpStatusCode responseCode = tokenServiceResponse.StatusCode;
                if (tokenServiceResponse.StatusCode == HttpStatusCode.OK)
                {
                    mResult.Result = Newtonsoft.Json.JsonConvert.DeserializeObject<AccessTokenModel>(responseString); ;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "";
                }
                else
                {
                    mResult.Message = tokenServiceResponse.RequestMessage.ToString();
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser user = await _userManager.FindByEmailAsync(model.EmailId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid email address.";
            }
            else
            {
                bool validRole = await _userManager.IsInRoleAsync(user.Id, user.UserRole.ToString());

                if (validRole)
                {
                    bool validPassword = await _userManager.CheckPasswordAsync(user, model.Password);
                    if (validPassword)
                    {
                        await SignInAsync(user, false, false);

                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = string.Format("User {0} Logged in successfully!", user.UserName);
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
            }
            return mResult;
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent, bool rememberBrowser)
        {
            WebLogout();
            ClaimsIdentity userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("FullName", user.UserName));
            _authenticationManager.SignIn( new AuthenticationProperties { IsPersistent = isPersistent },userIdentity);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(model.UserId);
                if (user == null)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid username or password.";
                }
                else
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Password changed successfully";
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public void WebLogout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
               DefaultAuthenticationTypes.TwoFactorCookie);
        }

        public IQueryable<UserDetailsModel> getAllUsers_IQueryable()
        {
            return (from u in _ctx.Users
                    from r in u.Roles
                    join role in _ctx.Roles on r.RoleId equals role.Id
                    where role.Name != UserRoles.Admin.ToString()
                    select new UserDetailsModel
                    {
                        Id = u.Id,
                        FullName = u.FirstName,
                        Email = u.Email,
                        ProfileImageUrl = u.ProfileImageName,
                        UserLanguage = u.Language,
                        IsActive = u.IsActive
                    }).OrderBy(x => x.FullName);
        }

        public ResponseModel<bool> ActiveToggle(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            ApplicationUser user = _ctx.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.IsActive = !user.IsActive;
                _ctx.SaveChanges();

                mResult.Status = ResponseStatus.Success;
                if (user.IsActive)
                {
                    mResult.Message = "Activated successfully.";
                }
                else
                {
                    mResult.Message = "Deactivated successfully.";
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = "User not found.";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Delete(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ApplicationUser user = _ctx.Users.FirstOrDefault(p => p.Id == UserId);
                IQueryable<ContactUs> Contact = _ctx.ContactUs.Where(p => p.UserId == UserId);
                _ctx.ContactUs.RemoveRange(Contact);

                IQueryable<UserNotifications> usernotifications = _ctx.UserNotifications.Where(p => p.ReceiverID == UserId);
                _ctx.UserNotifications.RemoveRange(usernotifications);

                IQueryable<UserPurchaseSongs> userpurchasesongs = _ctx.UserPurchaseSongs.Where(p => p.UserId == UserId);
                _ctx.UserPurchaseSongs.RemoveRange(userpurchasesongs);

                IQueryable<UserSongsDownload> usersongsdownload = _ctx.UserSongsDownload.Where(p => p.UserId == UserId);
                _ctx.UserSongsDownload.RemoveRange(usersongsdownload);

                IQueryable<UserRequestSongs> userrequestsongs = _ctx.UserRequestSongs.Where(p => p.UserId == UserId);
                _ctx.UserRequestSongs.RemoveRange(userrequestsongs);

                IQueryable<ContactUs> contactus = _ctx.ContactUs.Where(p => p.UserId == UserId);
                _ctx.ContactUs.RemoveRange(contactus);

                _ctx.Users.Remove(user);

                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "User deleted successfully.";
            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
