﻿using Songy.Core.Model;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class ContactusRepository:IDisposable
    {
        private ApplicationDbContext _ctx;

        public ContactusRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }

        public IQueryable<ContactUsViewModel> getContactUs_IQueryable()
        {
            return (from a in _ctx.ContactUs
                    orderby a.CreatedDate descending
                    select new ContactUsViewModel
                    {
                        Message = a.Message,
                        Name = a.User.FirstName,
                        DateTime = a.CreatedDate,
                    });
        }
    }
}
