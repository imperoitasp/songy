﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Core.Resources;
using Songy.Data.Entities;
using Songy.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Songy.Data.Repository
{
    public class SongsRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public SongsRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<SongsViewModel> GetIQueryableSongs()
        {
            IQueryable<SongsViewModel> query = (from songs in _ctx.Songs
                                                where songs.IsDeleted == false
                                                orderby songs.CreatedDate descending
                                                select new SongsViewModel
                                                {
                                                    Id = songs.Id,
                                                    Title = songs.Title,
                                                    CategoryId = songs.CategoryId,
                                                    CategoryName = songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == Language.English).CategoryName,
                                                    SubCategoryName = songs.SubCategory.SubCategoryLanguage.FirstOrDefault().Name,
                                                    Lyrics = songs.Lyrics,
                                                    ImageUrl = GlobalConfig.SongsImageUrl + songs.ImageName,
                                                    SongsUrl = GlobalConfig.SongsUrl + songs.FileName,
                                                    //ImageName = songs.ImageName,
                                                    //FileName = songs.FileName,
                                                    Example = songs.Example,
                                                    SongIdAndroid = songs.SongIdAndroid,
                                                    SongIdIos = songs.SongIdIos,
                                                    CreatedDate = songs.CreatedDate,
                                                    TotalDownload = _ctx.UserSongsDownload.Count(x => x.RequestSongs.SongsId == songs.Id),
                                                    TotalRequested = songs.RequestSongs.Count(),
                                                    LanguageId = songs.LanguageId,
                                                    IsFree = songs.IsFree,
                                                    HashTag = songs.HashTag.Select(x => new SongHashTagModel
                                                    {
                                                        Id = x.Id,
                                                        HashTag = x.HashTag

                                                    }).ToList()
                                                });

            //if (query != null)
            //{
            //    objsongs = query.Select(p => new SongsViewModel()
            //    {
            //        Id = p.Id,
            //        CategoryId = p.CategoryId,
            //        CategoryName = p.CategoryName,
            //        Example = p.Example,
            //        ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
            //        SongsUrl = GlobalConfig.SongsUrl + p.FileName,
            //        LanguageId = p.LanguageId,
            //        Lyrics = p.Lyrics,
            //        SongIdAndroid = p.SongIdAndroid,
            //        SongIdIos = p.SongIdIos,
            //        Title = p.Title,
            //        TotalDownload = p.TotalDownload,
            //        TotalRequested = p.TotalRequested,
            //        CreatedDate = p.CreatedDate,
            //        IsFree = p.IsFree,
            //        HashTag = p.HashTag.Select(x => new SongHashTagModel
            //        {
            //            Id = x.Id,
            //            HashTag = x.HashTag

            //        }).ToList()
            //    });
            //    return objsongs;
            //}
            return query;

        }

        public async Task<SongsAddUpdateModel> GetSongsDetail(long Id)
        {
            return await (from p in _ctx.Songs
                          where p.Id == Id
                          && p.IsDeleted == false
                          select new SongsAddUpdateModel
                          {
                              Id = p.Id,
                              CategoryId = p.CategoryId,
                              Example = p.Example,
                              imageName = p.ImageName,
                              FileName = p.FileName,
                              LanguageId = p.LanguageId,
                              Lyrics = p.Lyrics,
                              SongIdAndroid = p.SongIdAndroid,
                              SubCategoryId = p.SubCategoryId,
                              SongIdIos = p.SongIdIos,
                              Title = p.Title,
                              IsFree = p.IsFree,
                              SoongHashTag = p.HashTag.Select(x => new SongHashTagModel
                              {
                                  Id = x.Id,
                                  HashTag = x.HashTag

                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                Songs entity = await _ctx.Songs.FirstOrDefaultAsync(x => x.Id == Id);

                if (entity != null)
                {
                    entity.IsDeleted = true;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "songs deleted successfully!";
                }
                else
                {
                    mResult.Message = "No songs found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> AddUpdate(SongsAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                foreach (SongHashTagModel item in model.SoongHashTag)
                {
                    if (item.HashTag != null)
                    {
                        bool IsExist = model.Lyrics.Contains(item.HashTag);
                        if (!IsExist)
                        {
                            mResult.Status = ResponseStatus.Failed;
                            mResult.Message = string.Format("This Hash Tag {0} Not Found in this Songs Lyrics", item.HashTag);
                            return mResult;
                        }
                    }
                }
                Songs entity = await _ctx.Songs.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (entity == null)
                {
                    entity = new Songs();
                    mResult.Message = "Song record added successfully";
                }
                else
                {
                    mResult.Message = "Song record updated successfully";
                }

                entity.ImageName = model.imageName ?? entity.ImageName;
                entity.FileName = model.FileName ?? entity.FileName;
                entity.CategoryId = model.CategoryId;
                entity.SubCategoryId = model.SubCategoryId;
                entity.SongIdAndroid = model.SongIdAndroid == null ? string.Empty : model.SongIdAndroid.Trim();
                entity.SongIdIos = model.SongIdIos == null ? string.Empty : model.SongIdIos.Trim();
                entity.Example = model.Example;
                entity.Lyrics = model.Lyrics;
                entity.LanguageId = model.LanguageId;
                entity.Title = model.Title;
                entity.IsFree = model.IsFree;

                if (entity.HashTag == null)
                {
                    entity.HashTag = new List<MasterSongsHashTag>();
                }
                if (entity.HashTag.Any())
                {
                    _ctx.MasterSongsHashTag.RemoveRange(entity.HashTag);
                }
                if (model.SoongHashTag != null)
                {
                    foreach (SongHashTagModel item in model.SoongHashTag)
                    {
                        if (!string.IsNullOrWhiteSpace(item.HashTag))
                        {
                            entity.HashTag.Add(new MasterSongsHashTag
                            {
                                HashTag = item.HashTag,
                                Id = entity.Id
                            });
                        }
                    }
                }
                if (entity.Id == 0)
                {
                    _ctx.Songs.Add(entity);
                }
                try
                {
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
                catch (DbEntityValidationException err)
                {
                    foreach (DbEntityValidationResult validationErrors in err.EntityValidationErrors)
                    {
                        foreach (DbValidationError validationError in validationErrors.ValidationErrors)
                        {
                            if (!string.IsNullOrWhiteSpace(mResult.Message))
                            {
                                mResult.Message += Environment.NewLine;
                            }
                            mResult.Message += $"Property: {validationError.PropertyName}, Error: {validationError.ErrorMessage}";
                        }
                    }
                }
                catch (Exception err)
                {
                    mResult.Message = err.Message;
                }

                #region Send Notification
                if (entity.Id == 0)
                {

                    List<ApplicationUser> Users = await _ctx.Users.Where(x => x.UserRole == UserRoles.MobileUser).ToListAsync();
                    foreach (ApplicationUser User in Users)
                    {
                        string Message = string.Empty;
                        if (User.Language == Language.English)
                        {
                            Message = string.Format("<div style='font-family: Gotham-Book;font-size: 18px!important;'><font color='#fdba63'>{0}</font> is now available to download.</div>", entity.Title);
                        }
                        else
                        {
                            Message = string.Format("<div align='right' style='font-family: Gotham-Book;font-size: 18px!important;'>.الآن جاهز للتنزيل <font color='#fdba63'>{0}</font></div>", entity.Title);
                        }

                        await new NotificationRepository()
                  .SendNotification(NotificationType.NewSongsAdded, entity.Id.ToString(), Message, User.Id, model.UserId, entity.Id);
                    }

                }
                #endregion

            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> GetTrendingSongsRequestsList(TrendingSongsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);

            var query = await (from songs in _ctx.Songs
                               where songs.IsDeleted == false
                               select new
                               {
                                   Id = songs.Id,
                                   Title = songs.Title,
                                   CategoryId = songs.CategoryId,
                                   CategoryName = songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == loggedInUser.Language).CategoryName,
                                   CategoryImageName = songs.Category.IconName,
                                   Lyrics = songs.Lyrics,
                                   ImageName = songs.ImageName,
                                   FileName = songs.FileName,
                                   Example = songs.Example,
                                   SongIdAndroid = songs.SongIdAndroid,
                                   SongIdIos = songs.SongIdIos,
                                   CreatedDate = songs.CreatedDate,
                                   IsFree = songs.IsFree,
                                   TotalDownload = _ctx.UserSongsDownload.Count(x => x.RequestSongs.SongsId == songs.Id),
                                   TotalPurchase = songs.RequestSongs.Count(p => p.UserId == model.UserId),
                                   LanguageId = songs.LanguageId,
                                   HashTag = songs.HashTag.Select(x => new SongHashTagModel
                                   {
                                       Id = x.Id,
                                       HashTag = x.HashTag
                                   })
                                   .ToList()
                               })
                               .OrderByDescending(p => p.TotalDownload)
                               .Take(20)
                               .ToListAsync();


            if (query.Count > 0)
            {
                List<TrendingSongsRequestViewModel> Trending = query.Select(p => new TrendingSongsRequestViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    Example = p.Example,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + p.FileName,
                    CategoryImageUrl = GlobalConfig.CategoryIconUrl + p.CategoryImageName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    SongIdAndroid = p.SongIdAndroid,
                    SongIdIos = p.SongIdIos,
                    Title = p.Title,
                    TotalDownload = p.TotalDownload,
                    TotalPurchase = p.TotalPurchase,
                    CreatedDate = p.CreatedDate,
                    IsFree = p.IsFree,
                    HashTag = p.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag
                    }).ToList()
                }).ToList();

                switch (model.SortBy)
                {
                    case TrendingSortBy.MostPurchase:
                        Trending = Trending.OrderByDescending(p => p.TotalPurchase).ToList();
                        break;
                    case TrendingSortBy.Recent:
                        Trending = Trending.OrderByDescending(p => p.CreatedDate).ToList();
                        break;
                    default:
                        Trending = Trending.OrderByDescending(p => p.CreatedDate).ToList();
                        break;
                }

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.RecordListed;
                mResult.Result = Trending;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
                mResult.Result = new object[] { };
            }
            return mResult;

        }

        public async Task<ResponseModel<object>> GetMasterSongsDetails(SongsDetailsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);

            TrendingSongsRequestViewModel query = await (from songs in _ctx.Songs
                                                         where songs.Id == model.SongId
                                                         select new TrendingSongsRequestViewModel
                                                         {
                                                             Id = songs.Id,
                                                             Title = songs.Title,
                                                             CategoryId = songs.CategoryId,
                                                             CategoryName = songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == loggedInUser.Language).CategoryName,
                                                             Lyrics = songs.Lyrics,
                                                             ImageUrl = GlobalConfig.SongsImageUrl + songs.ImageName,
                                                             SongsUrl = GlobalConfig.SongsUrl + songs.FileName,
                                                             Example = songs.Example,
                                                             SongIdAndroid = songs.SongIdAndroid,
                                                             SongIdIos = songs.SongIdIos,
                                                             CreatedDate = songs.CreatedDate,
                                                             TotalDownload = _ctx.UserSongsDownload.Count(x => x.RequestSongs.SongsId == songs.Id),
                                                             TotalPurchase = songs.RequestSongs.Count(p => p.UserId == model.UserId),
                                                             LanguageId = songs.LanguageId,
                                                             IsFree = songs.IsFree,
                                                             HashTag = songs.HashTag.Select(x => new SongHashTagModel
                                                             {
                                                                 Id = x.Id,
                                                                 HashTag = x.HashTag
                                                             })
                                                             .ToList()
                                                         })
                               .FirstOrDefaultAsync();
            if (query == null)
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
                return mResult;
            }

            mResult.Status = ResponseStatus.Success;
            mResult.Message = ResponseMessages.RecordListed;
            mResult.Result = query;
            return mResult;
        }

        public async Task<ResponseModel<object>> GetRequestedSongsDetails(SongsDetailsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);

            var query = await (from req in _ctx.UserPurchaseSongs
                               where req.RequestedSongsId == model.SongId
                               select new
                               {
                                   Id = req.RequestedSongsId,
                                   Title = req.UserRequestSongs.Songs.Title,
                                   CategoryId = req.UserRequestSongs.Songs.CategoryId,
                                   CategoryName = req.UserRequestSongs.Songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == loggedInUser.Language).CategoryName,
                                   UserTag = req.HashTag.Select(x => x.HashTag).ToList(),
                                   Lyrics = req.UserRequestSongs.Lyrics,
                                   ImageName = req.UserRequestSongs.Songs.ImageName,
                                   FileName = req.UserRequestSongs.FileName,
                                   SongIdAndroid = req.UserRequestSongs.Songs.SongIdAndroid,
                                   SongIdIos = req.UserRequestSongs.Songs.SongIdIos,
                                   CreatedDate = req.UserRequestSongs.CreatedDate,
                                   LanguageId = req.UserRequestSongs.Songs.LanguageId,
                                   Isuploaded = req.UserRequestSongs.IsUploaded,
                                   IsFree = req.UserRequestSongs.Songs.IsFree,
                                   HashTag = req.UserRequestSongs.HashTag.Select(x => new SongHashTagModel
                                   {
                                       Id = x.Id,
                                       HashTag = x.Name
                                   }).ToList()
                               }).FirstOrDefaultAsync();

            if (query == null)
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
                return mResult;
            }
            else
            {
                MySongsDownloadRequestViewModel objData = new MySongsDownloadRequestViewModel()
                {
                    Id = query.Id,
                    Title = query.Title,
                    CategoryId = query.CategoryId,
                    CategoryName = query.CategoryName,
                    UserTag = string.Join(" & ", query.UserTag.ToList()),
                    Lyrics = query.Lyrics,
                    ImageUrl = GlobalConfig.SongsImageUrl + query.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + query.FileName,
                    SongIdAndroid = query.SongIdAndroid,
                    SongIdIos = query.SongIdIos,
                    CreatedDate = query.CreatedDate,
                    LanguageId = query.LanguageId,
                    Isuploaded = query.Isuploaded,
                    IsFree = query.IsFree,
                    HashTag = query.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag
                    }).ToList()
                };
                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.RecordListed;
                mResult.Result = objData;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> GetSongsByCategoryRequestsList(SongsByCategoryRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            long SubCategoryId = 0;
            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);

            List<SubCategoryFilterResultModel> SubCategoryList = await (from x in _ctx.SubCategory
                                                                        where x.CategoryId == model.CategoryId
                                                                        && (model.SubCategoryId == 0 || model.SubCategoryId == x.Id)
                                                                        && x.Songs.Any()
                                                                        let SubcategoryLang = x.SubCategoryLanguage.FirstOrDefault(y => y.Language == loggedInUser.Language)
                                                                        select new SubCategoryFilterResultModel
                                                                        {
                                                                            Id = x.Id,
                                                                            Name = SubcategoryLang.Name != null ? SubcategoryLang.Name : string.Empty
                                                                        }).ToListAsync();

            if (SubCategoryList != null && SubCategoryList.Count > 0)
            {
                SubCategoryId = model.SubCategoryId == 0 ? SubCategoryList.FirstOrDefault().Id : model.SubCategoryId;
            }
            if (SubCategoryList == null && SubCategoryList.Count == 0)
                SubCategoryList = new List<SubCategoryFilterResultModel>();


            var query = await (from songs in _ctx.Songs
                               where songs.IsDeleted == false
                               && songs.CategoryId == model.CategoryId
                               && (songs.SubCategoryId == SubCategoryId || model.SubCategoryId == 0)
                               select new
                               {
                                   Id = songs.Id,
                                   Title = songs.Title,
                                   CategoryId = songs.CategoryId,
                                   CategoryName = songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == loggedInUser.Language).CategoryName,
                                   Lyrics = songs.Lyrics,
                                   ImageName = songs.ImageName,
                                   FileName = songs.FileName,
                                   Example = songs.Example,
                                   SongIdAndroid = songs.SongIdAndroid,
                                   SongIdIos = songs.SongIdIos,
                                   CreatedDate = songs.CreatedDate,
                                   TotalDownload = _ctx.UserSongsDownload.Count(x => x.RequestSongs.SongsId == songs.Id),
                                   TotalPurchase = songs.RequestSongs.Count(p => p.UserId == model.UserId),
                                   LanguageId = songs.LanguageId,
                                   songs.IsFree,
                                   HashTag = songs.HashTag.Select(x => new SongHashTagModel
                                   {
                                       Id = x.Id,
                                       HashTag = x.HashTag
                                   })
                                   .ToList()
                               })
                               .OrderByDescending(p => p.CreatedDate)
                               .Skip(((model.PageIndex - 1) * model.PageSize))
                               .Take(model.PageSize)
                               .ToListAsync();

            if (query.Count > 0)
            {
                List<TrendingSongsRequestViewModel> songs = query.Select(p => new TrendingSongsRequestViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    Example = p.Example,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + p.FileName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    SongIdAndroid = p.SongIdAndroid,
                    SongIdIos = p.SongIdIos,
                    Title = p.Title,
                    IsFree = p.IsFree,
                    TotalDownload = p.TotalDownload,
                    TotalPurchase = p.TotalPurchase,
                    CreatedDate = p.CreatedDate,
                    HashTag = p.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag
                    }).ToList()
                }).ToList();

                switch (model.SortBy)
                {
                    case TrendingSortBy.MostPurchase:
                        songs = songs.OrderByDescending(p => p.TotalPurchase).ToList();
                        SubCategoryList.Add(new SubCategoryFilterResultModel() { Name = TrendingSortBy.MostPurchase.ToString(), Songs = songs });
                        break;
                    case TrendingSortBy.Recent:
                        songs = songs.OrderByDescending(p => p.CreatedDate).ToList();
                        SubCategoryList.Add(new SubCategoryFilterResultModel() { Name = TrendingSortBy.Recent.ToString(), Songs = songs });
                        break;
                    case TrendingSortBy.All:
                        songs = songs.OrderByDescending(p => p.CreatedDate).ToList();
                        SubCategoryList.Add(new SubCategoryFilterResultModel() { Name = TrendingSortBy.All.ToString(), Songs = songs });
                        break;
                    default:
                        songs = songs.OrderByDescending(p => p.CreatedDate).ToList();
                        if (SubCategoryList != null && SubCategoryList.Count == 0)
                        {

                        }
                        SubCategoryList.Add(new SubCategoryFilterResultModel() { Songs = songs, Name = SubCategoryList != null && SubCategoryList.Count > 0 ? SubCategoryList.FirstOrDefault().Name : "Trending" });
                        break;
                }


                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.RecordListed;
                mResult.Result = SubCategoryList;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;

        }

        public async Task<ResponseModel<object>> GetSongsBySearchRequestsList(SongsBySearchRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);

            var query = await (from songs in _ctx.Songs
                               where songs.Title.Contains(model.SearchText)
                               && songs.IsDeleted == false
                               select new
                               {
                                   Id = songs.Id,
                                   Title = songs.Title,
                                   CategoryId = songs.CategoryId,
                                   CategoryName = songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == loggedInUser.Language).CategoryName,
                                   CategoryImageName = songs.Category.IconName,
                                   Lyrics = songs.Lyrics,
                                   ImageName = songs.ImageName,
                                   FileName = songs.FileName,
                                   Example = songs.Example,
                                   SongIdAndroid = songs.SongIdAndroid,
                                   SongIdIos = songs.SongIdIos,
                                   CreatedDate = songs.CreatedDate,
                                   TotalDownload = _ctx.UserSongsDownload.Count(x => x.RequestSongs.SongsId == songs.Id),
                                   TotalPurchase = songs.RequestSongs.Count(x => x.UserId == model.UserId),
                                   LanguageId = songs.LanguageId,
                                   songs.IsFree,
                                   HashTag = songs.HashTag.Select(x => new SongHashTagModel
                                   {
                                       Id = x.Id,
                                       HashTag = x.HashTag
                                   })
                                   .ToList()
                               })
                               .OrderByDescending(p => p.CreatedDate)
                               .Skip(((model.PageIndex - 1) * model.PageSize))
                               .Take(model.PageSize)
                               .ToListAsync();

            if (query.Count > 0)
            {
                List<TrendingSongsRequestViewModel> songs = query.Select(p => new TrendingSongsRequestViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    Example = p.Example,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = GlobalConfig.SongsUrl + p.FileName,
                    CategoryImageUrl = GlobalConfig.CategoryIconUrl + p.CategoryImageName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    SongIdAndroid = p.SongIdAndroid,
                    SongIdIos = p.SongIdIos,
                    Title = p.Title,
                    IsFree = p.IsFree,
                    TotalDownload = p.TotalDownload,
                    TotalPurchase = p.TotalPurchase,
                    CreatedDate = p.CreatedDate,
                    HashTag = p.HashTag.Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag
                    }).ToList()
                }).ToList();

                switch (model.SortBy)
                {
                    case TrendingSortBy.MostPurchase:
                        songs = songs.OrderByDescending(p => p.TotalPurchase).ToList();
                        break;
                    case TrendingSortBy.Recent:
                        songs = songs.OrderByDescending(p => p.CreatedDate).ToList();
                        break;
                    default:
                        songs = songs.OrderByDescending(p => p.CreatedDate).ToList();
                        break;
                }

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.RecordListed;
                mResult.Result = songs;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
                mResult.Result = new object[] { };
            }
            return mResult;

        }

        public async Task<ResponseModel<object>> SongDownload(SongDownloadRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool objSongs = _ctx.UserRequestSongs.Any(p => p.Id == model.SongId);
            if (objSongs)
            {
                bool IsAlredy = _ctx.UserSongsDownload.Any(p => p.UserId == model.UserId && p.RequestSongsId == model.SongId);
                if (!IsAlredy)
                {
                    UserSongsDownload songsDownload = new UserSongsDownload()
                    {
                        RequestSongsId = model.SongId,
                        UserId = model.UserId
                    };
                    _ctx.UserSongsDownload.Add(songsDownload);
                    await _ctx.SaveChangesAsync();
                }
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Please enter correct Song Id";
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> MySongsDownload(MySongsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);

            var query = await (from req in _ctx.UserPurchaseSongs
                               where req.UserId == model.UserId
                               select new
                               {
                                   Id = req.UserRequestSongs.SongsId,
                                   RequestSongId = req.UserRequestSongs.Id,
                                   Title = req.UserRequestSongs.Songs.Title,
                                   CategoryId = req.UserRequestSongs.Songs.CategoryId,
                                   CategoryName = req.UserRequestSongs.Songs.Category.CategoryLanguage.FirstOrDefault(x => x.Language == loggedInUser.Language).CategoryName,
                                   UserTag = req.HashTag.Select(x => x.HashTag).ToList(),
                                   Lyrics = req.UserRequestSongs.Lyrics,
                                   ImageName = req.UserRequestSongs.Songs.ImageName,
                                   FileName = req.UserRequestSongs.FileName,
                                   SongIdAndroid = req.UserRequestSongs.Songs.SongIdAndroid,
                                   SongIdIos = req.UserRequestSongs.Songs.SongIdIos,
                                   CreatedDate = req.UserRequestSongs.CreatedDate,
                                   LanguageId = req.UserRequestSongs.Songs.LanguageId,
                                   IsUploaded = req.UserRequestSongs.IsUploaded,
                                   isFree = req.UserRequestSongs.Songs.IsFree,
                                   //HashTag = req.UserRequestSongs.HashTag.Select(x => new SongHashTagModel
                                   //{
                                   //    Id = x.Id,
                                   //    HashTag = x.Name
                                   //}).ToList(),

                               })
                               .OrderByDescending(p => !p.IsUploaded).ThenByDescending(p => p.CreatedDate)
                               .Skip(((model.PageIndex - 1) * model.PageSize))
                               .Take(model.PageSize)
                               .ToListAsync();

            if (query.Count > 0)
            {
                List<MySongsDownloadRequestViewModel> Trending = query.Select(p => new MySongsDownloadRequestViewModel()
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    CategoryName = p.CategoryName,
                    ImageUrl = GlobalConfig.SongsImageUrl + p.ImageName,
                    SongsUrl = p.FileName != null ? GlobalConfig.RequestedSongsUrl + p.FileName : string.Empty,
                    SongName = p.FileName,
                    LanguageId = p.LanguageId,
                    Lyrics = p.Lyrics,
                    SongIdAndroid = p.SongIdAndroid,
                    SongIdIos = p.SongIdIos,
                    Title = p.Title,
                    IsFree = p.isFree,
                    UserTag = string.Join(" & ", p.UserTag.ToList()),
                    CreatedDate = p.CreatedDate,
                    Isuploaded = p.IsUploaded,
                    PermaLink = p.FileName != null ? GlobalConfig.SongsParmaLinkBaseUrl + p.FileName + "/" : string.Empty,
                    HashTag = _ctx.PurchaseSongHashTag.Where(x => x.PurchaseSongs.RequestedSongsId == p.RequestSongId).Select(x => new SongHashTagModel
                    {
                        Id = x.Id,
                        HashTag = x.HashTag
                    }).ToList()
                }).ToList();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.RecordListed;
                mResult.Result = Trending;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
                mResult.Result = new object[] { };
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SearchSongsAvailableForPurchase(PurchaseSearchSongRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.HashTag.ForEach(x => x.ToLower());
            ApplicationUser loggedInUser = await _ctx.Users.SingleOrDefaultAsync(p => p.Id == model.UserId);
            Songs masterSong = await _ctx.Songs.Include("HashTag").FirstOrDefaultAsync(p => p.Id == model.MasterSongId && p.IsDeleted == false);
            if (masterSong != null)
            {
                var objSearch = await (from req in _ctx.UserRequestSongs
                                       where req.HashTag.Select(x => x.Name.ToLower()).Intersect(model.HashTag).Count() == model.HashTag.Count
                                       && req.SongsId == model.MasterSongId
                                       select new
                                       {
                                           Id = req.Id,
                                           MasterSongId = req.SongsId,
                                       }).FirstOrDefaultAsync();
                if (objSearch != null)
                {
                    bool hasPurchased = await _ctx.UserPurchaseSongs.AnyAsync(x => x.RequestedSongsId == objSearch.Id && x.UserId == model.UserId);
                    if (hasPurchased)
                    {
                        mResult.Status = ResponseStatus.SongsAlreadyBuy;
                        mResult.Message = ResponseMessages.SongsAlreadyBuy;
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = ResponseMessages.SongAvailableForPurchase;
                        if (masterSong.IsFree)
                        {
                            mResult.Message = ResponseMessages.SongAvailableForDownload;
                        }

                        mResult.Result = new SearchSongResponceModel
                        {
                            MasterSongId = objSearch.MasterSongId,
                            SongId = objSearch.Id,
                            IsFree = masterSong.IsFree,
                            IsMasterSong = false
                        };
                    }
                }
                else
                {
                    objSearch = await (from req in _ctx.Songs
                                       where req.HashTag.Select(x => x.HashTag.ToLower()).Intersect(model.HashTag).Count() == model.HashTag.Count
                                       && req.Id == model.MasterSongId
                                       && req.IsDeleted == false
                                       select new
                                       {
                                           Id = req.Id,
                                           MasterSongId = req.Id
                                       }).FirstOrDefaultAsync();
                    if (objSearch != null)
                    {
                        bool hasPurchased = await (from req in _ctx.UserRequestSongs
                                                   where
                                                   req.HashTag.Select(x => x.Name.ToLower()).Intersect(model.HashTag)
                                                   .Count() == model.HashTag.Count
                                                   && req.SongsId == model.MasterSongId
                                                   && req.UserId == model.UserId
                                                   select req).AnyAsync();
                        if (hasPurchased)
                        {
                            mResult.Status = ResponseStatus.SongsAlreadyBuy;
                            mResult.Message = ResponseMessages.SongsAlreadyBuy;
                        }
                        else
                        {
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = ResponseMessages.SongAvailableForPurchase;
                            if (masterSong.IsFree)
                            {
                                mResult.Message = ResponseMessages.SongAvailableForDownload;
                            }

                            mResult.Result = new SearchSongResponceModel
                            {
                                MasterSongId = objSearch.MasterSongId,
                                SongId = objSearch.Id,
                                IsFree = masterSong.IsFree,
                                IsMasterSong = true
                            };
                        }
                    }
                    else
                    {
                        mResult.Message = ResponseMessages.SongNotAvailableForPurchase;
                        if (masterSong.IsFree)
                        {
                            mResult.Message = ResponseMessages.SongNotAvailableForDownload;
                        }

                        mResult.Status = ResponseStatus.SongsNotAvailable;
                        mResult.Result = new SearchSongResponceModel
                        {
                            MasterSongId = model.MasterSongId,
                            IsFree = masterSong.IsFree,
                        };
                    }
                }
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> PurchaseSongs(PurchaseSongRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Songs songs = _ctx.Songs.Include("HashTag").FirstOrDefault(p => p.Id == model.MasterSongId);
            if (songs != null)
            {
                bool IsExist = await _ctx.UserRequestSongs.AnyAsync(p => p.Id == model.SongId);
                if (IsExist)
                {
                    UserPurchaseSongs purchase = new UserPurchaseSongs()
                    {
                        Price = model.Price,
                        UserId = model.UserId,
                        RequestedSongsId = model.SongId,
                        Token = model.Token,
                    };

                    List<PurchaseSongHashTag> purchasesonghashtag = new List<PurchaseSongHashTag>();

                    foreach (string item in model.HashTag)
                    {
                        if (!string.IsNullOrWhiteSpace(item))
                        {
                            purchasesonghashtag.Add(new PurchaseSongHashTag
                            {
                                HashTag = item,
                                Id = purchase.Id
                            });
                        }
                    }

                    purchase.HashTag = purchasesonghashtag;
                    _ctx.UserPurchaseSongs.Add(purchase);
                    await _ctx.SaveChangesAsync();
                }
                else
                {
                    if (model.IsMasterSong)
                    {
                        try
                        {
                            string ExistsPath = GlobalConfig.SongsPath + songs.FileName;
                            FileInfo file = new FileInfo(ExistsPath);
                            string DestPath = GlobalConfig.RequestedSongsPath + songs.FileName;
                            GC.Collect();
                            if (File.Exists(ExistsPath))
                            {
                                file.CopyTo(DestPath, true);
                            }
                            List<string> Oldtag = songs.HashTag.Select(x => x.HashTag).ToList();
                            List<string> Newtag = model.HashTag.ToList();

                            string NewLyrics = songs.Lyrics;
                            for (int i = 0; i < songs.HashTag.Count; i++)
                            {
                                NewLyrics = NewLyrics.Replace(Oldtag[i], Newtag[i]);
                            }

                            UserRequestSongs userRequestSongs = new UserRequestSongs()
                            {
                                Lyrics = NewLyrics,
                                SongsId = model.MasterSongId,
                                UserId = model.UserId,
                                IsUploaded = true,
                                FileName = songs.FileName,
                            };

                            List<SongHashTag> songhashtag = new List<SongHashTag>();

                            foreach (string item in model.HashTag)
                            {
                                if (!string.IsNullOrWhiteSpace(item))
                                {
                                    songhashtag.Add(new SongHashTag
                                    {
                                        Name = item,
                                        RequestSongsId = userRequestSongs.Id
                                    });
                                }
                            }

                            userRequestSongs.HashTag = songhashtag;
                            _ctx.UserRequestSongs.Add(userRequestSongs);
                            UserPurchaseSongs purchase = new UserPurchaseSongs()
                            {
                                Price = model.Price,
                                UserId = model.UserId,
                                RequestedSongsId = userRequestSongs.Id,
                                Token = model.Token
                            };

                            List<PurchaseSongHashTag> purchasesonghashtag = new List<PurchaseSongHashTag>();

                            foreach (string item in model.HashTag)
                            {
                                if (!string.IsNullOrWhiteSpace(item))
                                {
                                    purchasesonghashtag.Add(new PurchaseSongHashTag
                                    {
                                        HashTag = item,
                                        Id = purchase.Id
                                    });
                                }
                            }

                            purchase.HashTag = purchasesonghashtag;
                            _ctx.UserPurchaseSongs.Add(purchase);
                            await _ctx.SaveChangesAsync();
                            mResult.Status = ResponseStatus.Success;
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                            {
                                foreach (DbValidationError ve in eve.ValidationErrors)
                                {
                                    mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            mResult.Status = ResponseStatus.Failed;
                            mResult.Message = ex.Message;
                        }
                    }
                    else
                    {
                        try
                        {
                            List<string> Oldtag = songs.HashTag.Select(x => x.HashTag).ToList();
                            List<string> Newtag = model.HashTag.ToList();

                            string NewLyrics = songs.Lyrics;
                            for (int i = 0; i < songs.HashTag.Count; i++)
                            {
                                NewLyrics = NewLyrics.Replace(Oldtag[i], Newtag[i]);
                            }

                            UserRequestSongs userRequestSongs = new UserRequestSongs()
                            {
                                Lyrics = NewLyrics,
                                SongsId = model.MasterSongId,
                                UserId = model.UserId,
                            };

                            List<SongHashTag> songhashtag = new List<SongHashTag>();

                            foreach (string item in model.HashTag)
                            {
                                if (!string.IsNullOrWhiteSpace(item))
                                {
                                    songhashtag.Add(new SongHashTag
                                    {
                                        Name = item,
                                        RequestSongsId = userRequestSongs.Id
                                    });
                                }
                            }

                            userRequestSongs.HashTag = songhashtag;
                            _ctx.UserRequestSongs.Add(userRequestSongs);
                            UserPurchaseSongs purchase = new UserPurchaseSongs()
                            {
                                Price = model.Price,
                                UserId = model.UserId,
                                RequestedSongsId = userRequestSongs.Id,
                                Token = model.Token
                            };

                            List<PurchaseSongHashTag> purchasesonghashtag = new List<PurchaseSongHashTag>();

                            foreach (string item in model.HashTag)
                            {
                                if (!string.IsNullOrWhiteSpace(item))
                                {
                                    purchasesonghashtag.Add(new PurchaseSongHashTag
                                    {
                                        HashTag = item,
                                        Id = purchase.Id
                                    });
                                }
                            }

                            purchase.HashTag = purchasesonghashtag;
                            _ctx.UserPurchaseSongs.Add(purchase);
                            await _ctx.SaveChangesAsync();
                            mResult.Status = ResponseStatus.Success;
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                            {
                                foreach (DbValidationError ve in eve.ValidationErrors)
                                {
                                    mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            mResult.Status = ResponseStatus.Failed;
                            mResult.Message = ex.Message;
                        }
                    }
                }
                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.PurchaseSuccess;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = ResponseMessages.DataNotFound;
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
