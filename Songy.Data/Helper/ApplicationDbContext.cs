﻿using Microsoft.AspNet.Identity.EntityFramework;
using Songy.Data.Entities;
using Songy.Data.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Helper
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("connectionString")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<IdentityRole>().ToTable("RoleMaster");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
        }


        #region User entities

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<CategoryLanguage> CategoryLanguage { get; set; }
        public virtual DbSet<SubCategory> SubCategory { get; set; }
        public virtual DbSet<SubCategoryLanguage> SubCategoryLanguage { get; set; }
        public virtual DbSet<Songs> Songs { get; set; }
        public virtual DbSet<UserLoginTransaction> UserLoginTransaction { get; set; }
        public virtual DbSet<UserPurchaseSongs> UserPurchaseSongs { get; set; }
        public virtual DbSet<UserRequestSongs> UserRequestSongs { get; set; }
        public virtual DbSet<UserSongsDownload> UserSongsDownload { get; set; }
        public virtual DbSet<SongHashTag> SongHashTag { get; set; }
        public virtual DbSet<ContactUs> ContactUs { get; set; }
        public virtual DbSet<ApplicationSetting> ApplicationSetting { get; set; }
        public virtual DbSet<MasterSongsHashTag> MasterSongsHashTag { get; set; }
        public virtual DbSet<PurchaseSongHashTag> PurchaseSongHashTag { get; set; }


        #endregion
        public virtual DbSet<TermsSongy> TermsSongy { get; set; }
        public virtual DbSet<AboutSongy> AboutSongy { get; set; }

        public virtual DbSet<UserNotifications> UserNotifications { get; set; }
        //public virtual DbSet<NotificationTypes> NotificationTypes { get; set; }


    }
}
