﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songy.Data.Helper
{
    public static class Utility
    {
        public static DateTime GetSystemDateTimeUTC()
        {
            return DateTime.UtcNow;
        }
        public static string GenerateEmailBody(EmailTemplate templateFileName, Dictionary<string, string> dicPlaceholders)
        {
            dicPlaceholders.Add("{{projectName}}", GlobalConfig.ProjectName);
            string templatePath = Path.Combine(GlobalConfig.EmailTemplatePath, templateFileName.ToString() + ".html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(templatePath))
            {
                body = reader.ReadToEnd();
            }
            if (body.Length > 0)
            {
                foreach (var item in dicPlaceholders)
                {
                    body = body.Replace(item.Key, item.Value);
                }
            }
            return body;
        }


    }
}
