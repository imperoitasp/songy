﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Songy.Admin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContextWrapper context = new HttpContextWrapper(Context);
            Exception exception = Server.GetLastError();
            HttpException httpException = exception as HttpException ?? new HttpException(500, exception.Message);
            Server.ClearError();

            IController controller = new Controllers.ErrorController();
            HttpContext.Current.Response.ContentType = "text/html";
            Response.StatusCode = httpException.GetHttpCode();
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            switch (Response.StatusCode)
            {
                case 404:// page not found
                    routeData.Values.Add("action", "NotFound");
                    break;
                case 403:// Bad Request
                    routeData.Values.Add("action", "BadRequest");
                    routeData.Values.Add("errorTitle", exception.Message);
                    routeData.Values.Add("errorDescription", exception.InnerException?.Message);
                    break;
                case 500:// server error
                    routeData.Values.Add("action", "ServerError");
                    routeData.Values.Add("errorTitle", exception.Message);
                    routeData.Values.Add("errorDescription", exception.InnerException?.Message);
                    break;
                case 401:// unauthorised error
                    routeData.Values.Add("action", "Unauthorized");
                    break;
                default: // default error
                    routeData.Values.Add("action", "Index");
                    routeData.Values.Add("errorTitle", exception.Message);
                    routeData.Values.Add("errorDescription", exception.InnerException?.Message);
                    break;
            }
            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}
