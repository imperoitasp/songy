﻿using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class AccountController : Controller
    {
        private UserService _service;
        public AccountController()
        {
            _service = new UserService();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            AdminLoginModel model = new AdminLoginModel();
            if (User.Identity.IsAuthenticated)
                return RedirectToActionPermanent("Index", "Home");
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AdminLoginModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<object> mResult = await _service.AdminLogin(model);
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToAction("Index", "Home");
                ModelState.AddModelError("", mResult.Message);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(AdminChangePasswordModel model)
        {
            _service = new UserService(Request.GetOwinContext());
            ResponseModel<object> mResult = await _service.AdminChangePassword(model);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger, mResult.Message);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            _service = new UserService(Request.GetOwinContext());
            _service.WebLogout();
            return RedirectToActionPermanent("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string passwordResetToken, string emailID)
        {
            string decodeToken = HttpUtility.UrlDecode(passwordResetToken);
            ResetPasswordModel model = new ResetPasswordModel()
            {
                PasswordResetToken = decodeToken,
                EmailID = emailID
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = new ResponseModel<object>();
                try
                {
                    mResult = await _service.ResetPassword(model);
                }
                catch (Exception ex)
                {
                    mResult.Message = ex.Message;
                }

                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("ResetPasswordConfirmed");
                }
                else
                {
                    TempData["alertModel"] = new AlertModel(AlertStatus.Danger, mResult.Message);
                }
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmed()
        {
            return View();
        }
    }
}