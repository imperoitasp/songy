﻿using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly UtilityService _service;
        public HomeController()
        {

            _service = new UtilityService();
        }
        public async Task<ActionResult> Index()
        {
            AdminDashboardModel model = await _service.GetAddminDashboard();
            return View(model);
        }

    }
}