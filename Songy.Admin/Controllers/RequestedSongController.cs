﻿using Microsoft.AspNet.Identity;
using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class RequestedSongController : ApplicationBaseController
    {
        private RequestedSongService _service;

        ResponseModel<object> mResult;
        public RequestedSongController()
        {
            _service = new RequestedSongService();
        }

        public ActionResult Pending()
        {
            ViewBag.Title = "Pending";
            IQueryable<SongsViewModel> objSongs = _service.GetIQueryablePendingSongs();
            return View("Index", objSongs);
        }

        public ActionResult RequestedBySongs(long Id)
        {
            IQueryable<SongsViewModel> objSongs = _service.RequestedBySongs(Id);
            ViewBag.Title = objSongs.FirstOrDefault()?.Title;
            return View("Index", objSongs);
        }
        public ActionResult RequestedSongsDownloadByUser(long Id)
        {
            IQueryable<UserDetailsModel> objUser = _service.RequestedSongsDownloadByUser(Id);
            ViewBag.Title = objUser.FirstOrDefault()?.Title;
            return View(objUser);
        }

        public ActionResult Uploaded()
        {
            ViewBag.Title = "Uploaded";
            IQueryable<SongsViewModel> objSongs = _service.GetIQueryableUploadedSongs();
            return View("Index", objSongs);
        }

        public async Task<ActionResult> Add(long Id)
        {
            BindCategory();
            BindLanguage();
            RequestedSongsAddUpdateModel model = await _service.GetRequestedSongsDetail(Id);
            if (model == null)
                return RedirectToActionPermanent("Pending");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(RequestedSongsAddUpdateModel model)
        {
            BindCategory();
            BindLanguage();
            if (ModelState.IsValid)
            {
                if (model.FileBaseSongs != null)
                {
                    string ogFileName = model.FileBaseSongs.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.FileBaseSongs.SaveAs(Path.Combine(GlobalConfig.RequestedSongsPath, thisFileName));
                    model.FileName = thisFileName;
                }
                model.UserId = User.Identity.GetUserId();
                mResult = await _service.InsertOrUpdateRequestedSongs(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Uploaded");

            }
            return View("AddorUpdate", model);
        }
    }
}