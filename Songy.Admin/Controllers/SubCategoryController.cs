﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class SubCategoryController : ApplicationBaseController
    {
        private SubCategoryService _service;

        public SubCategoryController()
        {
            _service = new SubCategoryService();
        }

        public ActionResult Index()
        {
            IQueryable<SubCategoryListModel> objCategories = _service.GetAllSubCategory_IQueryable();
            return View(objCategories);
        }

        public ActionResult Add()
        {
            SubCategoryAddUpdateRequestModel model = new SubCategoryAddUpdateRequestModel();
            List<SubCategoryLanguageModel> SubCategoryLanguages = new List<SubCategoryLanguageModel>();
            SubCategoryLanguages.Add(new SubCategoryLanguageModel()
            {
                Language = Language.English,
                LanguageName = Language.English.ToString(),
            });

            SubCategoryLanguages.Add(new SubCategoryLanguageModel()
            {
                Language = Language.Arbic,
                LanguageName = Language.Arbic.ToString(),
            });
         
            model.Languages = SubCategoryLanguages;
            BindCategory(false);
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(SubCategoryAddUpdateRequestModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<SubCategoryAddUpdateResultModel> mResult = await _service.AddUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            BindCategory(false);
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            SubCategoryAddUpdateRequestModel model = await _service.GetSubCategoryDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            BindCategory(false);
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(SubCategoryAddUpdateRequestModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<SubCategoryAddUpdateResultModel> mResult = await _service.AddUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");

            }
            BindCategory(false);
            return View("AddorUpdate", model);
        }

        public async Task<JsonResult> Delete(long Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckExistingName()
        {
            bool IsExist = true;

            if (Request.Form[0] != null && Request.Form[1] != null && Request.Form[2] != null)
            {
                string Name = Request.Form[0];
                int CategoryId = Convert.ToInt32(Request.Form[1]);
                int SubCategoryId = Convert.ToInt32(Request.Form[2]);
                IsExist = _service.IsNameExist(Name, CategoryId, SubCategoryId);
            }

            return Json(!IsExist);
        }
    }
}