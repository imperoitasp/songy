﻿using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class ContactUsController : Controller
    {
        private readonly ContactusService _service;

        public ContactUsController()
        {
            _service = new ContactusService();
        }
        public ActionResult Index()
        {
            IQueryable<ContactUsViewModel> objData = _service.getContactUs_IQueryable();
            return View(objData);
        }
    }
}