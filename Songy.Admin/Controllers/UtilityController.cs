﻿using Microsoft.AspNet.Identity;
using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class UtilityController : ApplicationBaseController
    {
        private readonly UtilityService _service;
        public UtilityController()
        {

            _service = new UtilityService();
        }
        public ActionResult Terms()
        {
            UtilityModel model = _service.TermsCondition();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Terms(UtilityModel model)
        {
            int id = await _service.UpdateTermsConditions(model);
            if (id < 1)
            {
                OnBindMessage(AlertStatus.Danger, "Failed to update terms and conditions!");
                ModelState.AddModelError("", "Failed to update terms and conditions!");
            }
            else
            {
                OnBindMessage(AlertStatus.Success, "Terms and conditions updated successfully.");

            }
            return RedirectToAction("Terms");
        }

        public ActionResult AboutUs()
        {
            UtilityModel model = _service.About();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AboutUs(UtilityModel model)
        {
            int id = await _service.Updateabout(model);
            if (id < 1)
            {
                OnBindMessage(AlertStatus.Danger, "Failed to update AboutUs!");
                ModelState.AddModelError("", "Failed to update AboutUs!");
            }
            else
            {
                OnBindMessage(AlertStatus.Success, "AboutUs updated successfully.");
            }
            return RedirectToAction("AboutUs");
        }

        [HttpPost]
        public JsonResult UploadCropImage(SaveImageModel model)
        {
            string imageName = Guid.NewGuid().ToString() + "." + model.extension;
            string filePath = Path.Combine(model.saveFilePath, imageName);
            ResponseModel<object> mResult = Utilities.SaveCropImage(model.imgBase64String, filePath, model.extension);
            mResult.Result = imageName;

            if (!string.IsNullOrEmpty(model.previousImageName))
            {
                string ExistFilePath = Path.Combine(model.saveFilePath, model.previousImageName);
                if (System.IO.File.Exists(ExistFilePath))
                {
                    System.IO.File.Delete(ExistFilePath);
                }
            }

            return Json(mResult);
        }

        public ActionResult Notification()
        {
            BindPlatform();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Notification(NotificationSendModel model)
        {
            model.SenderID = User.Identity.GetUserId();
            NotificationService service = new NotificationService();
            ResponseModel<object> mResult = await service.NotificationSend(model);
            OnBindMessage(mResult.Status, mResult.Message);
            //if (mResult.Status == ResponseStatus.Success)
            return RedirectToAction("Index", "Home");
        }
    }
}