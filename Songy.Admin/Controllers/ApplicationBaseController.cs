﻿using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class ApplicationBaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (User != null)
            //{
            //    ClaimsIsdentity userIdentity = (ClaimsIdentity)User.Identity;
            //    if (userIdentity != null)
            //    {
            //        IEnumerable<Claim> claims = userIdentity.Claims;
            //        if (claims.Any())
            //        {
            //            ViewBag.FullName = claims.Where(c => c.Type == "FullName").FirstOrDefault()?.Value;
            //        }
            //    }
            //}
            OnInitBreadcrumb();
        }

        public void OnBindMessage(ResponseStatus status, string message)
        {
            OnBindMessage((status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger), message);
        }

        public void OnBindMessage(AlertStatus status, string message)
        {
            TempData["alertModel"] = new AlertModel(status, message);
        }

        public void OnInitBreadcrumb()
        {
            string actionName = ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = ControllerContext.RouteData.Values["controller"].ToString();

            if (controllerName != "Home")
            {
                if (actionName == "Index")
                {
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        subActionTitle = controllerName
                    };
                }
                else
                {
                    ViewBag.Action = actionName;
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        actioncontroller = controllerName,
                        actionName = "Index",
                        actionTitle = controllerName,
                        subActionTitle = actionName
                    };
                }
            }
        }

        public void BindCategory(bool FromSong=true)
        {
            using (CategoryService service = new CategoryService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetCategoryDropdown(FromSong).ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.CategoryName,
                                                         Value = x.CategoryId.ToString()
                                                     }).ToList();
                ViewData["CategoryList"] = dropDownList;
            }
        }
        public void BindSubCategory(long CategoryId = 0)
        {
            using (SubCategoryService service = new SubCategoryService())
            {
                List<SelectListItem> Subcategories = (from x in service.GetSubCategoryDropdown(CategoryId)
                                                                select new SelectListItem
                                                                {
                                                                    Text = x.Name,
                                                                    Value = x.Id.ToString()
                                                                }).ToList();
                ViewData["SubCategoryList"] = Subcategories;
            }
        }
        public void BindLanguage()
        {
            var Languagelist = new List<SelectListItem>();
            Languagelist.Add(new SelectListItem { Text = Language.English.ToString(), Value = Language.English.ToString() });
            Languagelist.Add(new SelectListItem { Text = Language.Arbic.ToString(), Value = Language.Arbic.ToString() });

            ViewData["LanguageList"] = Languagelist;
        }
        public void BindPlatform()
        {
            var PlatformList = new List<SelectListItem>();
            PlatformList.Add(new SelectListItem { Text = "All", Value = Platform.Web.ToString() });
            PlatformList.Add(new SelectListItem { Text = Platform.Android.ToString(), Value = Platform.Android.ToString() });
            PlatformList.Add(new SelectListItem { Text = Platform.IOS.ToString(), Value = Platform.IOS.ToString() });
            ViewData["PlatformList"] = PlatformList;
        }

        public void BindList(long CategoryId = 0)
        {
            BindCategory(true);
            BindSubCategory(CategoryId);
            BindLanguage();
        }
    }
}