﻿using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class CategoryController : ApplicationBaseController
    {
        private CategoryService _service;

        ResponseModel<object> mResult;

        public CategoryController()
        {
            _service = new CategoryService();
        }

        public ActionResult Index()
        {
            IQueryable<AdminCategoryModel> objCategories = _service.GetAllCategory_IQueryable();
            return View(objCategories);
        }

        public ActionResult Add()
        {
            AdminCategoryModel model = new AdminCategoryModel();
            List<CategoryLanguageModel> CategoryLanguages = new List<CategoryLanguageModel>();
            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                Language = Language.English,
                LanguageName = Language.English.ToString(),
            });

            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                Language = Language.Arbic,
                LanguageName = Language.Arbic.ToString(),
            });

            model.Categorylanguagemodellist = CategoryLanguages;
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AdminCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.FileBaseIconCategory != null)
                {
                    Stream inStream = model.FileBaseIconCategory.InputStream;
                    Image PostedImage = Image.FromStream(inStream);

                    string ogFileName = model.FileBaseIconCategory.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.FileBaseIconCategory.SaveAs(Path.Combine(GlobalConfig.CategoryIconPath, thisFileName));
                    model.IconName = thisFileName;
                }

                mResult = await _service.AddUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");

            }
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            AdminCategoryModel model = await _service.GetCategoryDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(AdminCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.FileBaseIconCategory != null)
                {
                    Stream inStream = model.FileBaseIconCategory.InputStream;
                    Image PostedImage = Image.FromStream(inStream);

                    string ogFileName = model.FileBaseIconCategory.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.FileBaseIconCategory.SaveAs(Path.Combine(GlobalConfig.CategoryIconPath, thisFileName));
                    model.IconName = thisFileName;
                }

                mResult = await _service.AddUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }

        public async Task<JsonResult> Delete(long Id)
        {
            mResult = await _service.DeleteCategory(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckExistingTitle()
        {
            bool IsExist = true;

            if (Request.Form[0] != null)
            {
                string Name = Request.Form[0];
                long CategoryId = Convert.ToInt64(Request.Form[1]);
                IsExist = _service.IsTitleExist(Name, CategoryId);
            }

            return Json(!IsExist);
        }
    }
}