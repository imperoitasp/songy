﻿using Songy.Core.Enumerations;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class AppSettingsController : ApplicationBaseController
    {
        ApplicationService _service;
        private ResponseModel<object> mResult;

        public AppSettingsController()
        {
            _service = new ApplicationService();
            mResult = new ResponseModel<object>();
        }

        public async Task<ActionResult> Index()
        {
            ApplicationSettingModel model = await _service.GetAppSettingAsync();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(ApplicationSettingModel model)
        {
            if (ModelState.IsValid)
            {
                mResult = await _service.UpdateSettings(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
                else
                    ModelState.AddModelError("", mResult.Message);
            }
            return View(model);
        }
    }
}
