﻿using Microsoft.AspNet.Identity;
using Songy.Core.Enumerations;
using Songy.Core.Helper;
using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class SongsController : ApplicationBaseController
    {
        private SongsService _service;

        ResponseModel<object> mResult;

        public SongsController()
        {
            _service = new SongsService();
        }

        public ActionResult Index()
        {
            IQueryable<SongsViewModel> objSongs = _service.GetIQueryableSongs();
            return View(objSongs);
        }

        public async Task<ActionResult> Add()
        {
            SongsAddUpdateModel model = new SongsAddUpdateModel();
            BindList();
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(SongsAddUpdateModel model)
        {
          
            if (ModelState.IsValid)
            {
                if (model.FileBaseSongs != null)
                {
                    string ogFileName = model.FileBaseSongs.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.FileBaseSongs.SaveAs(Path.Combine(GlobalConfig.SongsPath, thisFileName));
                    model.FileName = thisFileName;
                }
                model.UserId = User.Identity.GetUserId();
                mResult = await _service.AddUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");

            }
            BindList();
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            SongsAddUpdateModel mResult = await _service.GetSongsDetail(id);
            BindList(mResult.CategoryId);
            if (mResult == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", mResult);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(SongsAddUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.FileBaseSongs != null)
                {
                    string ogFileName = model.FileBaseSongs.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                    model.FileBaseSongs.SaveAs(Path.Combine(GlobalConfig.SongsPath, thisFileName));
                    model.FileName = thisFileName;
                }

                mResult = await _service.AddUpdate(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            BindList();
            return View("AddorUpdate", model);
        }

        public async Task<JsonResult> Delete(long Id)
        {
            mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubCategory(int CategoryId)
        {

            using (SubCategoryService service = new SubCategoryService())
            {
                List<SubCategoryDropdownModel> Subcategories = (from x in service.GetSubCategoryDropdown(CategoryId)
                                                                select new SubCategoryDropdownModel
                                                                {
                                                                    Id = x.Id,
                                                                    Name = x.Name
                                                                }).ToList();
                return Json(Subcategories, JsonRequestBehavior.AllowGet);
            }
        }

    }
}