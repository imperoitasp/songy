﻿using Songy.Core.Model;
using Songy.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Songy.Admin.Controllers
{
    public class UserController : Controller
    {

        private UserService _service;
        public UserController()
        {
            _service = new UserService();
        }

        public ActionResult Index()
        {
            return View(_service.getAllUsers_IQueryable());
        }

        [HttpPost]
        public JsonResult ActiveToggle(string Id)
        {
            ResponseModel<bool> mResult = _service.ActiveToggle(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(string Id)
        {
            ResponseModel<object> mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> ForgotPasswordByAdmin(string Id)
        {
            ResponseModel<object> mResult = await _service.ForgotPasswordByAdmin(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}